<?php

namespace Craft\Kernel;

class Router
{
    public static $prefix = ['u', 'profile'];

    /**
     * 解析参数 /u/username/controller/action
     * @param $pathInfo
     * @return array
     * @throws Exception
     */
    public static function parse($pathInfo)
    {
        if ($pathInfo[0] == static::$prefix[0]) {
            switch (count($pathInfo)) {
                case 2:
                    $controller = 'index';
                    $action = 'index';
                    break;
                case 3:
                    $controller = $pathInfo[2];
                    $action = 'index';
                    break;
                case 4:
                    $controller = $pathInfo[2];
                    $action = $pathInfo[3];
                    break;
                default:
                    throw new Exception('路由错误');
            }
            $_GET['nicknamex'] = $pathInfo[1];
        } elseif ($pathInfo[0] == static::$prefix[2]) {
            switch (count($pathInfo)) {
                case 1:
                    $controller = 'index';
                    $action = 'index';
                    break;
                case 2:
                    $controller = $pathInfo[1];
                    $action = 'index';
                    break;
                case 3:
                    $controller = $pathInfo[1];
                    $action = $pathInfo[2];
                    break;
                default:
                    throw new Exception('路由错误');
            }
        } else {
            throw new Exception('路由错误');
        }
        return [
            'controller' => $controller,
            'action' => $action
        ];
    }
}