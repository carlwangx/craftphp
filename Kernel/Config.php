<?php
namespace Craft\Kernel;

class Config
{
    private static $config = [];   // 配置参数
    private static $range = '_sys_';   // 参数作用域

    /**
     * 检测配置是否存在
     * @param $name
     * @param string $range
     * @return bool
     */
    public static function has($name, $range = '')
    {
        $range = $range ? $range : self::$range;
        $name = strtolower($name);

        if (!strpos($name, '.')) {
            return isset(self::$config[$range][$name]);
        } else {
            // 二维数组设置和获取支持
            $name = explode('.', $name);
            return isset(self::$config[$range][$name[0]][$name[1]]);
        }
    }

    /**
     * 获取配置参数 为空则获取所有配置
     * @param null $name
     * @param string $range
     * @return mixed|null
     */
    public static function get($name = null, $range = '')
    {
        $range = $range ? $range : self::$range;
        // 无参数时获取所有
        if (empty($name)) {
            return self::$config[$range];
        }
        $name = strtolower($name);
        if (!strpos($name, '.')) {
            // 判断环境变量
            if (isset($_ENV[$name])) {
                return $_ENV[$name];
            }
            return isset(self::$config[$range][$name]) ? self::$config[$range][$name] : null;
        } else {
            // 二维数组设置和获取支持
            $name = explode('.', $name);
            // 判断环境变量
            if (isset($_ENV[$name[0] . '_' . $name[1]])) {
                return $_ENV[$name[0] . '_' . $name[1]];
            }
            return isset(self::$config[$range][$name[0]][$name[1]]) ? self::$config[$range][$name[0]][$name[1]] : null;
        }
    }

    /**
     * 设置配置参数 name为数组则为批量设置
     * @param $name
     * @param null $value
     * @param string $range
     * @throws Exception
     */
    public static function set($name, $value = null, $range = '')
    {
        $range = $range ? $range : self::$range;
        if (empty(self::$config[$range])) {
            self::$config[$range] = [];
        }
        if (is_string($name)) {
            $name = strtolower($name);
            if (!strpos($name, '.')) {
                self::$config[$range][$name] = $value;
            } else {
                // 二维数组设置和获取支持
                $name = explode('.', $name);
                self::$config[$range][$name[0]][$name[1]] = $value;
            }
        } elseif (is_array($name)) {
            // 如果配置的值为数组，则合并配置项的值
            foreach ($name as $key => $value) {
                if (is_array($value) && !empty(self::$config[$range][$key])) {
                    $name[$key] = array_merge(self::$config[$range][$key], $value);
                }
            }
            // 批量设置
            self::$config[$range] = array_merge(self::$config[$range], array_change_key_case($name));
        } else {
            throw new Exception('非法参数');
        }
    }

    /**
     * 获取某个作用域的配置列表
     * @param $range
     * @return mixed|null
     */
    public static function getRange($range)
    {
        return isset(self::$config[$range]) ? self::$config[$range] : null;
    }
}
