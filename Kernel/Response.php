<?php

namespace Craft\Kernel;

class Response
{
    /**
     * Ajax方式返回数据到客户端
     * @access protected
     * @param $code
     * @param array $data
     * @param string $msg
     * @param String $type AJAX返回数据格式
     * @param mixed $fun 数据处理方法
     * @return void
     */
    public function ajaxReturn($code, $data = [], $msg = '', $type = 'json', $fun = '')
    {
        $result = [
            'code' => $code,
            'msg' => $msg,
            'data' => $data
        ];
        if (empty($type)) {
            $type = C('default_ajax_return');
        }
        $headers = [
            'json' => 'application/json',
            'jsonp' => 'application/javascript',
            'script' => 'application/javascript',
            'html' => 'text/html',
            'text' => 'text/plain',
        ];
        $type = strtolower($type);
        if (isset($headers[$type])) {
            header('Content-Type:' . $headers[$type] . '; charset=utf-8');
        }
        if ($fun && is_callable($fun)) {
            $result = call_user_func($fun, $result);
        } else {
            switch ($type) {
                case 'json':
                    // 返回JSON数据格式到客户端 包含状态信息
                    $result = json_encode($result);
                    break;
                case 'jsonp':
                    // 返回JSON数据格式到客户端 包含状态信息
                    $handler = isset($_GET[C('var_jsonp_handler')]) ? $_GET[C('var_jsonp_handler')] : C('default_jsonp_handler');
                    $result = $handler . '(' . json_encode($result) . ');';
                    break;
                default:
                    // 返回JSON数据格式到客户端 包含状态信息
                    $result = json_encode($result);
            }
        }
        exit($result);
    }

    /**
     * @param $name
     * @param string $value
     * @return mixed
     */
    public function cookie($name, $value = '')
    {
        if (is_null($name)) { // 清除
            Cookie::clear($value);
        } elseif ('' === $value) { // 获取
            return Cookie::get($name);
        } elseif (is_null($value)) { // 删除session
            Cookie::delete($name);
        } else { // 设置session
            Cookie::set($name, $value);
        }
    }

    /**
     * @param $name
     * @param string $value
     * @return mixed
     */
    public function session($name, $value = '')
    {
        if (is_null($name)) { // 清除
            Session::clear($value);
        } elseif ('' === $value) { // 获取
            return Session::get($name);
        } elseif (is_null($value)) { // 删除session
            Session::delete($name);
        } else { // 设置session
            Session::set($name, $value);
        }
    }

    /**
     * 发送HTTP状态
     * @param integer $code 状态码
     * @return void
     */
    public function sendHttpStatus($code)
    {
        static $_status = array(
            // Informational 1xx
            100 => 'Continue',
            101 => 'Switching Protocols',
            // Success 2xx
            200 => 'OK',
            201 => 'Created',
            202 => 'Accepted',
            203 => 'Non-Authoritative Information',
            204 => 'No Content',
            205 => 'Reset Content',
            206 => 'Partial Content',
            // Redirection 3xx
            300 => 'Multiple Choices',
            301 => 'Moved Permanently',
            302 => 'Moved Temporarily ',  // 1.1
            303 => 'See Other',
            304 => 'Not Modified',
            305 => 'Use Proxy',
            // 306 is deprecated but reserved
            307 => 'Temporary Redirect',
            // Client Error 4xx
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            406 => 'Not Acceptable',
            407 => 'Proxy Authentication Required',
            408 => 'Request Timeout',
            409 => 'Conflict',
            410 => 'Gone',
            411 => 'Length Required',
            412 => 'Precondition Failed',
            413 => 'Request Entity Too Large',
            414 => 'Request-URI Too Long',
            415 => 'Unsupported Media Type',
            416 => 'Requested Range Not Satisfiable',
            417 => 'Expectation Failed',
            // Server Error 5xx
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
            502 => 'Bad Gateway',
            503 => 'Service Unavailable',
            504 => 'Gateway Timeout',
            505 => 'HTTP Version Not Supported',
            509 => 'Bandwidth Limit Exceeded'
        );
        if (isset($_status[$code])) {
            header('HTTP/1.1 ' . $code . ' ' . $_status[$code]);
            // 确保FastCGI模式下正常
            header('Status:' . $code . ' ' . $_status[$code]);
        }
    }
}