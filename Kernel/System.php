<?php
namespace Craft\Kernel;

class System
{
    public static function errorHandler()
    {
        if (APP_DEBUG) {
            ini_set('display_errors', 'on');
            error_reporting(-1);
        } else {
            error_reporting(E_ALL);
            $handlerClassName = 'Craft\\Kernel\\Error\\' . ucfirst(APP_MODE);
            set_exception_handler([$handlerClassName, 'exception']);
            set_error_handler([$handlerClassName, 'error']);
            register_shutdown_function([$handlerClassName, 'shutdown']);
        }
    }

    /**
     * 加载配置文件
     * 1 系统默认配置
     * 2 公共模块配置
     */
    public static function loadConfig()
    {
        // 系统默认配置
        Config::set(include FRAMEWORK_ROOT . 'config.php');
        // 项目配置文件
        Config::set(include APP_CONFIG . 'config.php');
        // 状态配置
        define('APP_STATUS', Config::get('app_status'));
        if (is_file(APP_CONFIG . APP_STATUS . '.php')) {
            Config::set(include APP_CONFIG . APP_STATUS . '.php');
        }
        // 模块配置
        if (defined('SAAS_CONFIG')) {
            // saas应用的配置
            Config::set(include APP_CONFIG . SAAS_CONFIG . '.php');
        }
        Config::set(include MODULE_PATH . 'Config/app.php');
    }

    /**
     * 加载公共函数
     * 1 系统公共函数
     * 2 公共模块函数
     */
    public static function loadFunction()
    {
        // 加载系统函数
        require FRAMEWORK_ROOT . 'helper.php';
    }
}
