<?php
namespace Craft\Kernel;

/**
 * Class Db
 */
class Db
{
    /** @var Db\Database $db  当前数据库操作对象 */
    protected $db;

    /** @var  Db\Builder\Builder 构建器 */
    protected $builder;

    protected static $instances = [];

    protected $fields = null;

    protected $pk = 'id';

    protected $options = [
//        'table' => null,
//        'join' => [],
//        'field' => true,
//        'where' => [],
//        'order' => [],
//        'limit' => [0, 0],
//        'group' => '',
//        'distinct' => '',
//        'union' => '',
//        'having' => '',
//        'lock' => '',
//        'force' => '',
    ];

    public $dryRun = false;

    public static function getInstance($config)
    {
        $configHash = md5(serialize($config));
        if (empty(static::$instances[$configHash])) {
            static::$instances[$configHash] = new static($config);
        }
        return static::$instances[$configHash];
    }


    private function __construct($config)
    {
        if ($config['driver'] == 'mysql') {
            $this->db = new Db\Mysql($config);
            $this->builder = new Db\Builder\Mysql();
        } else {
            $this->db = new Db\Pgsql($config);
            $this->builder = new Db\Builder\Pgsql();
        }
    }

    public function table($name)
    {
        // 重置信息
        $this->options = [
            'table' => $name,
            'join' => [],
            'field' => '*',
            'where' => [],
            'order' => [],
            'limit' => [0, 0],
            'group' => '',
            'distinct' => '',
            'union' => '',
            'having' => '',
            'lock' => '',
            'force' => '',
        ];
        $this->builder->resetBinds();
        return $this;
    }

    public function where($condition)
    {
        $this->options['where'] = $condition;
        return $this;
    }

    public function field($fields)
    {
        $this->options['field'] = $fields;
        return $this;
    }

    public function order($order)
    {
        $this->options['order'] = $order;
        return $this;
    }

    public function page($num, $size)
    {
        $offset = max(($num - 1) * $size - 1, 0);
        $this->options['limit'] = [$size, $offset];
        return $this;
    }

    public function limit($limit)
    {
        $this->options['limit'] = [$limit, 0];
        return $this;
    }

    public function insert($data)
    {
        $sql = $this->builder->insert($data, $this->options);
        $binds = $this->builder->binds();
        $result = $this->db->execute($sql, $binds, $this->dryRun);
        if ($result) {
            return $this->db->getLastInsertId();
        }
        return false;
    }

    /**
     * @todo
     * @param $dataSet
     * @return array|string
     */
    public function batchInsert($dataSet)
    {
        $sql = $this->builder->batchInsert($dataSet, $this->options);
        $binds = $this->builder->binds();
        $this->db->execute($sql, $binds, $this->dryRun);
    }

    public function find()
    {
        $this->options['limit'] = 1;
        $sql = $this->builder->select($this->options);
        $binds = $this->builder->binds();
        $result = $this->db->execute($sql, $binds, $this->dryRun);
        if ($result) {
            $fetchResult = $this->db->getFetchResult();
            return isset($fetchResult[0]) ? $fetchResult[0] : null;
        }
    }

    public function select()
    {
        $sql = $this->builder->select($this->options);
        $binds = $this->builder->binds();
        $result = $this->db->execute($sql, $binds, $this->dryRun);
        if ($result) {
            $this->db->execute($sql, $binds, $this->dryRun);
            return $this->db->getFetchResult();
        }
    }

    public function update($fields)
    {
        $sql = $this->builder->update($fields, $this->options);
        $binds = $this->builder->binds();
        $result = $this->db->execute($sql, $binds, $this->dryRun);
        if ($result) {
            return $this->db->getRowCount();
        }
    }

    public function setInc($field, $step)
    {
        $fields = [$field => ['exp', $field . '+' . $step]];
        $sql = $this->builder->update($fields, $this->options);
        $binds = $this->builder->binds();
        $result = $this->db->execute($sql, $binds, $this->dryRun);
        if ($result) {
            return $this->db->getRowCount();
        }
    }

    public function delete()
    {
        $sql = $this->builder->delete($this->options);
        $binds = $this->builder->binds();
        $result = $this->db->execute($sql, $binds, $this->dryRun);
        if ($result) {
            return $this->db->getRowCount();
        }
    }

    public function count()
    {
        $sql = $this->builder->count($this->options);
        $binds = $this->builder->binds();
        $result = $this->db->execute($sql, $binds, $this->dryRun);
        if ($result) {
            $fetchResult = $this->db->getFetchResult();
            return intval($fetchResult[0]['total']);
        }
    }

    public function query($sql, $binds)
    {
        $this->builder->resetBinds();
        $result = $this->db->execute($sql, $binds, $this->dryRun);
        if ($result) {
            $sql = explode(' ', $sql);
            $type = strtolower($sql[0]);
            if ($type == 'insert') {
                $result = $this->db->getLastInsertId();
            } elseif ($type == 'update' || $type == 'delete') {
                $result = $this->db->getRowCount();
            } else {
                $result = $this->db->getFetchResult();
            }
            return $result;
        }
    }

    public function beginTrans()
    {
        $this->db->beginTrans();
    }

    public function commit()
    {
        $this->db->commit();
    }

    public function rollback()
    {
        $this->db->rollback();
    }
}