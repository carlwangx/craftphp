<?php
namespace Craft\Kernel\Db\Builder;

abstract class Builder
{
    protected $bindParams = [];

    // 数据库表达式
    protected $exp = ['eq' => '=', 'neq' => '!=', 'gt' => '>', 'egt' => '>=', 'lt' => '<', 'elt' => '<=',
        'notlike' => 'NOT LIKE', 'like' => 'LIKE', 'in' => 'IN', 'exp' => 'EXP', 'notin' => 'NOT IN',
        'between' => 'BETWEEN', 'notbetween' => 'NOT BETWEEN', 'exists' => 'EXISTS', 'notexists' => 'NOT EXISTS',
        'null' => 'NULL', 'notnull' => 'NOT NULL', '> time' => '> TIME', '< time' => '< TIME', '>= time' => '>= TIME',
        '<= time' => '<= TIME', 'between time' => 'BETWEEN TIME', 'not between time' => 'NOT BETWEEN TIME',
        'notbetween time' => 'NOT BETWEEN TIME'];

    // SQL表达式
    protected $selectSql    = 'SELECT%DISTINCT% %FIELD% FROM %TABLE%%FORCE%%JOIN%%WHERE%%GROUP%%HAVING%%ORDER%%LIMIT% %UNION%%LOCK%';
    protected $insertSql    = '%INSERT% INTO %TABLE% (%FIELD%) VALUES (%DATA%)';
    protected $insertAllSql = 'INSERT INTO %TABLE% (%FIELD%) VALUES %DATA%';
    protected $updateSql    = 'UPDATE %TABLE% SET %SET% %JOIN% %WHERE% %ORDER%%LIMIT% %LOCK%';
    protected $deleteSql    = 'DELETE FROM %TABLE% %USING% %JOIN% %WHERE% %ORDER%%LIMIT% %LOCK%';


    public function binds()
    {
        return $this->bindParams;
    }

    public function resetBinds()
    {
        $this->bindParams = [];
    }

    public function insert($fields, $options, $replace = false)
    {
        // 分析并处理数据
        $data = $this->parseData($fields, $options);
        if (empty($data)) {
            return 0;
        }
        $fields = array_keys($data);
        $values = array_values($data);

        $sql = str_replace(
            ['%INSERT%', '%TABLE%', '%FIELD%', '%DATA%'],
            [
                $replace ? 'REPLACE' : 'INSERT',
                $this->parseTable($options['table']),
                implode(' , ', $fields),
                implode(' , ', $values),
            ], $this->insertSql);

        return $sql;
    }

    public function batchInsert($dataSet, $options)
    {
        $fields = $options['field'];
        foreach ($dataSet as &$data) {
            foreach ($data as $key => $val) {
                if (!in_array($key, $fields, true)) {
                    if ($options['strict']) {
                        E('fields not exists:[' . $key . ']');
                    }
                    unset($data[$key]);
                } elseif (is_null($val)) {
                    $data[$key] = 'NULL';
                } elseif (is_scalar($val)) {
                    $data[$key] = $this->parseValue($val, $key);
                } elseif (is_object($val) && method_exists($val, '__toString')) {
                    // 对象数据写入
                    $data[$key] = $val->__toString();
                } else {
                    // 过滤掉非标量数据
                    unset($data[$key]);
                }
            }
            $value    = array_values($data);
            $values[] = 'SELECT ' . implode(',', $value);
        }
        $fields = array_map([$this, 'parseKey'], array_keys(reset($dataSet)));
        $sql    = str_replace(
            ['%TABLE%', '%FIELD%', '%DATA%'],
            [
                $this->parseTable($options['table']),
                implode(' , ', $fields),
                implode(' UNION ALL ', $values),
            ], $this->insertAllSql);

        return $sql;
    }


    public function select($options)
    {
        $sql = str_replace(
            ['%TABLE%', '%DISTINCT%', '%FIELD%', '%JOIN%', '%WHERE%', '%GROUP%', '%HAVING%', '%ORDER%', '%LIMIT%', '%UNION%', '%LOCK%', '%FORCE%'],
            [
                $this->parseTable($options['table']),
                $this->parseDistinct($options['distinct']),
                $this->parseField($options['field'], $options),
                $this->parseJoin($options['join'], $options),
                $this->parseWhere($options['where'], $options),
                $this->parseGroup($options['group']),
                $this->parseHaving($options['having']),
                $this->parseOrder($options['order'], $options),
                $this->parseLimit($options['limit']),
                $this->parseUnion($options['union']),
                $this->parseLock($options['lock']),
                $this->parseForce($options['force']),
            ], $this->selectSql);
        return $sql;
    }

    public function count($options)
    {
        $sql = str_replace(
            ['%TABLE%', '%DISTINCT%', '%FIELD%', '%JOIN%', '%WHERE%', '%GROUP%', '%HAVING%', '%ORDER%', '%LIMIT%', '%UNION%', '%LOCK%', '%FORCE%'],
            [
                $this->parseTable($options['table']),
                $this->parseDistinct($options['distinct']),
                'COUNT(1) as total',
                $this->parseJoin($options['join'], $options),
                $this->parseWhere($options['where'], $options),
                $this->parseGroup($options['group']),
                $this->parseHaving($options['having']),
                $this->parseOrder($options['order'], $options),
                $this->parseLimit($options['limit']),
                $this->parseUnion($options['union']),
                $this->parseLock($options['lock']),
                $this->parseForce($options['force']),
            ], $this->selectSql);
        return $sql;
    }


    public function update($fields, $options)
    {
        $data  = $this->parseData($fields, $options);
        if (empty($data)) {
            return '';
        }
        foreach ($data as $key => $val) {
            $set[] = $key . '=' . $val;
        }
        $sql = str_replace(
            ['%TABLE%', '%SET%', '%JOIN%', '%WHERE%', '%ORDER%', '%LIMIT%', '%LOCK%'],
            [
                $this->parseTable($options['table'], $options),
                implode(',', $set),
                $this->parseJoin($options['join'], $options),
                $this->parseWhere($options['where'], $options),
                $this->parseOrder($options['order'], $options),
                $this->parseLimit($options['limit']),
                $this->parseLock($options['lock']),
            ], $this->updateSql);

        return $sql;
    }


    public function delete($options)
    {
        $sql = str_replace(
            ['%TABLE%', '%USING%', '%JOIN%', '%WHERE%', '%ORDER%', '%LIMIT%', '%LOCK%'],
            [
                $this->parseTable($options['table']),
                !empty($options['using']) ? ' USING ' . $this->parseTable($options['using'], $options) . ' ' : '',
                $this->parseJoin($options['join'], $options),
                $this->parseWhere($options['where'], $options),
                $this->parseOrder($options['order'], $options),
                $this->parseLimit($options['limit']),
                $this->parseLock($options['lock']),
            ], $this->deleteSql);

        return $sql;
    }

    /**
     * 数据分析
     * @access protected
     * @param array     $data 数据
     * @param array     $options 查询参数
     * @return array
     */
    protected function parseData($data, $options)
    {
        if (empty($data)) {
            return [];
        }
        $result = [];
        foreach ($data as $field => $value) {
            if (is_array($value)) {
                if ($value[0] == 'exp') {
                    $result[$field] = $value[1];
                }
            } else {
                $this->bindParams[$field] = $value;
                $result[$field] = ':' . $field;
            }
        }
        return $result;
    }


    private function parseHaving()
    {

    }

    private function parseUnion()
    {

    }


    private function parseGroup()
    {

    }


    private function parseDistinct()
    {

    }

    private function parseJoin()
    {

    }

    private function parseLock()
    {

    }

    private function parseForce()
    {

    }


    private function parseTable($table)
    {
        return "`{$table}`";
    }

    private function parseField($field)
    {
        if ($field === true) {
            $field = '*';
        }
        return $field;
    }

    private function parseWhere($where, $options)
    {
        if (empty($where)) {
            $where = [];
        }
        $whereArr = [];
        foreach ($where as $field => $value) {
            $whereArr[] = $this->parseWhereItem($field, $value);
        }
        $whereStr = '';
        if ($whereArr) {
            $whereStr = ' WHERE ' . implode(' AND ', $whereArr);
        }
        return $whereStr;
    }

    private function parseOrder($order)
    {
        $orderBy = [];
        foreach ($order as $key => $val) {
            $orderBy[] = $key . ' ' . $val;
        }
        if ($orderBy) {
            return ' ORDER BY ' . implode(',', $orderBy);
        }
    }

    private function parseLimit($limit)
    {
        $limitStr = '';
        if ($limit[0] > 0) {
            $limitStr = ' LIMIT ' . $limit[0];
            if ($limit[1] > 0) {
                $limitStr .= ' OFFSET ' . $limit[1];
            }
        }
        return $limitStr;
    }

    /**
     * where子单元分析
     * ['id' => 1, 'id' => 2], ['id' => 1, 'id' => ['in', [1,2,3]]]
     * @param $field
     * @param $value
     * @return string
     * @throws \Exception
     */
    protected function parseWhereItem($field, $value)
    {
        $whereStr = '';
        // 查询规则和条件
        if (!is_array($value)) {
            $value = ['eq', $value];
        }
        list($exp, $value) = $value;
        if ($exp == 'inset') {
            $whereStr =  "FIND_IN_SET({$value}, {$field})";
        } elseif (!isset($this->exp[$exp])) {
            throw new \Exception('where express error:' . $exp);
        } else {
            $whereStr = $field . ' ' . $this->exp[$exp] . ' :' . $field;
            $this->bindParams[$field] = $value;
        }
        return $whereStr;
    }
}