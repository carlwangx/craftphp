<?php
namespace Craft\Kernel\Db;

use PDO;
use PDOException;
use PDOStatement;

/**
 * PDO driver
 * Class Database
 */
abstract class Database
{
    public $binds = [];

    /** @var  PDOStatement PDO操作实例 */
    protected $pdoStatement;

    protected $instances = [];

    /** @var PDO pdo实例 */
    protected $pdo;

    // 当前SQL指令
    protected $sql;

    // 返回或者影响记录数
    protected $rowCount;

    // 事务指令数
    protected $transTimes = 0;

    protected static $executeTimes = 0;

    protected $params = [
        PDO::ATTR_CASE              => PDO::CASE_NATURAL,
        PDO::ATTR_ERRMODE           => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_ORACLE_NULLS      => PDO::NULL_NATURAL,
        PDO::ATTR_STRINGIFY_FETCHES => false,
        PDO::ATTR_EMULATE_PREPARES  => false,
    ];

    protected $options = [];

    public function __construct($config)
    {
        $dsn = $this->parseDsn($config);
        if (empty($config['options'])) {
            $options = $this->options;
        } else {
            $options = array_merge($this->options, $config['options']);
        }
        $this->pdo = new PDO($dsn, $config['username'], $config['password'], $options);
    }


    /**
     * @param $bind
     */
    public function bindValue($bind)
    {
        foreach ($bind as $key => $val) {
            // 占位符
            $param = ':' . $key;
            if (is_array($val)) {
                if (PDO::PARAM_INT == $val[1] && '' === $val[0]) {
                    $val[0] = 0;
                }
                $result = $this->pdoStatement->bindValue($param, $val[0], $val[1]);
            } else {
                $result = $this->pdoStatement->bindValue($param, $val);
            }
            if (!$result) {
                $msg = $this->pdoStatement->errorInfo();
                $error = [
                    'sql' => $this->getLastSql(),
                    'msg' => implode(',', $msg)
                ];
                throw new PDOException(json_encode($error));
            }
        }
    }


    /**
     * 查询 prepare -> bindParam -> execute
     * @param $sql
     * @param $binds
     * @param $dryRun
     * @return array|string
     */
    public function execute(string $sql, array $binds, bool $dryRun)
    {
        $this->sql = $sql;
        $this->binds = $binds;
        $lastSql = $this->getLastSql();
        if (!$dryRun) {
            static::$executeTimes++;
            $this->pdoStatement = $this->pdo->prepare($sql);
            $this->bindValue($binds);
            $result = $this->pdoStatement->execute();
            if (!$result) {
                $msg = $this->pdoStatement->errorInfo();
                $error = [
                    'sql' => $lastSql,
                    'msg' => implode(',', $msg)
                ];
                throw new PDOException(json_encode($error));
            }
            return true;
        } else {
            var_dump($lastSql);
            return false;
        }
    }


    public function getFetchResult()
    {
        $this->pdoStatement->setFetchMode(PDO::FETCH_ASSOC);
        return $this->pdoStatement->fetchAll();
    }

    public function getRowCount()
    {
        return $this->pdoStatement->rowCount();
    }

    public function getLastSql()
    {
        $sql = $this->sql;
        $binds = $this->binds;
        foreach ($binds as $key => $val) {
            $value = is_array($val) ? $val[0] : $val;
            $type  = is_array($val) ? $val[1] : PDO::PARAM_STR;
            if (PDO::PARAM_STR == $type) {
                $value = $this->pdo->quote($value);
            } elseif (PDO::PARAM_INT == $type) {
                $value = (float) $value;
            }
            // 判断占位符
            $sql = is_numeric($key) ?
                substr_replace($sql, $value, strpos($sql, '?'), 1) :
                str_replace(
                    [':' . $key . ')', ':' . $key . ',', ':' . $key . ' '],
                    [$value . ')', $value . ',', $value . ' '],
                    $sql . ' ');
        }
        return rtrim($sql);
    }


    public function getLastInsertId($name = null)
    {
        // PDO_PGSQL() 要求为 name 参数指定序列对象的名称
        return $this->pdo->lastInsertId($name);
    }

    // 事务说明：嵌套事务，都是采用savePoint方式
    // 这里不使用savePoint，判断是否用事务，如果有，则不commit或begin
    /**
     * 启动事务
     * @access public
     * @return void
     */
    public function beginTrans()
    {
        ++$this->transTimes;
        if ($this->transTimes == 1) {
            $this->pdo->beginTransaction();
        }
    }

    /**
     * 用于非自动提交状态下面的查询提交
     * @access public
     * @return void
     * @throws PDOException
     */
    public function commit()
    {
        if ($this->transTimes == 1) {
            $this->pdo->commit();
        }
        --$this->transTimes;
    }

    /**
     * 事务回滚
     * @access public
     * @return void
     * @throws PDOException
     */
    public function rollback()
    {
        if ($this->transTimes == 1) {
            $this->pdo->rollBack();
        }
        $this->transTimes = max(0, $this->transTimes - 1);
    }

    /**
     * 解析pdo连接的dsn信息
     * @access protected
     * @param array $config 连接信息
     * @return string
     */
    abstract protected function parseDsn($config);

    /**
     * 取得数据表的字段信息
     * @access public
     * @param string $tableName
     * @return array
     */
    abstract public function getFields($tableName);

    /**
     * 取得数据库的表信息
     * @access public
     * @param string $dbName
     * @return array
     */
    abstract public function getTables($dbName);

    /**
     * SQL性能分析
     * @access protected
     * @param string $sql
     * @return array
     */
    abstract protected function getExplain($sql);
}