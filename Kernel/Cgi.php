<?php
namespace Craft\Kernel;

class Cgi
{
    /**
     * URL调度，反射机制
     * Exception:
     * 1 控制器不存在
     * 2 方法不存在
     * @throws \ReflectionException
     * @throws \Exception
     */
    public static function start()
    {
        $config = Config::get();
        // 启动cookie
        Cookie::init($config['cookie']);
        // 启动session
        Session::init($config['session']);
        $request = parse_url($_SERVER['REQUEST_URI']);
        // 根目录
        if ($request['path'] == '/') {
            // 默认controller和action名称
            define('CONTROLLER_NAME', 'Index');
            define('ACTION_NAME', 'index');
        } else {
            // 大小写检查
            if ($request['path'] != strtolower($request['path'])) {
                new Exception('URL必须全为小写');
            }
            // URL不需要后缀
            $pathInfo = explode('.', $request['path']);
            // 路由检测和控制器、操作解析, 安全检测
            $url = static::parseUrl($pathInfo[0], $config);
            // 获取控制器名
            define('CONTROLLER_NAME', $url['controller']);
            // 获取操作名
            define('ACTION_NAME', $url['action']);
        }
        //保证$_REQUEST正常取值
        $_REQUEST = array_merge($_POST, $_GET, $_COOKIE);
        // 获取当前操作名
        $action = ACTION_NAME;
        $instance = Loader::controller(CONTROLLER_NAME);
        if (method_exists($instance, $action)) {
            //执行当前操作
            $method = new \ReflectionMethod($instance, $action);
            if ($method->isPublic()) {
                $method->invoke($instance);
            } else {
                // 操作方法不是Public 抛出异常
                throw new \ReflectionException($action . '必须是public');
            }
        } else {
            // 方法不存在
            throw new \ReflectionException($action . '方法不存在');
        }
    }

    /**
     * 解析模块的URL地址 控制器/操作[/参数]，多个单词使用'-'分隔
     * @param $url
     * @param $config
     * @return array
     * @throws Exception
     */
    public static function parseUrl($url, $config)
    {
        $result = [];
        $urlPrefix = '/' .  $config['url_prefix'];
        $paths = explode('/', ltrim($url, $urlPrefix));
        if (in_array($paths[0], Router::$prefix)) {
            $result = Router::parse($paths);
        } else {
            $result['controller'] = ucfirst(name_url_trans(array_shift($paths), 1));
            if ($paths) {
                $result['action'] = name_url_trans(array_shift($paths), 1);
                if (!empty($paths)) {
                    $GLOBALS[$config['url_params']['name']] = implode('/', $paths);
                } else {
                    $GLOBALS[$config['url_params']['name']] = '';
                }
            } else {
                $result['action'] = '_route';
            }
        }
        return $result;
    }
}
