<?php
namespace Craft\Kernel\View;

use Craft\Kernel\Config;
use Craft\Kernel\Exception;

/**
 * 模板编译
 */
class  Template
{
    protected $cache = null;

    protected $config = [];

    private $block = [];

    /**
     * 架构函数
     * @access public
     */
    public function __construct()
    {
        $this->config = Config::get('template');
        $this->config['taglib_begin'] = $this->stripPreg($this->config['taglib_begin']);
        $this->config['taglib_end'] = $this->stripPreg($this->config['taglib_end']);
        $this->config['tpl_begin'] = $this->stripPreg($this->config['tpl_begin']);
        $this->config['tpl_end'] = $this->stripPreg($this->config['tpl_end']);
        // 初始化模板缓存器
        $this->cache = new Cache();
    }

    /**
     * 渲染模板文件
     * @access public
     * @param string $template 模板文件
     * @param array $vars 模板变量
     * @return void
     * @throws Exception
     */
    public function display($template, $vars = [])
    {
        $template = $this->parseTemplateFile($template);
        $cacheFile = $this->config['cache_path'] . md5($template . Config::get('app_version')) . '.php';
        $this->checkCache($template, $cacheFile); // 检查缓存
        // 页面缓存
        ob_start();
        ob_implicit_flush(0);
        // 读取编译存储
        $this->cache->read($cacheFile, $vars);
        // 获取并清空缓存
        $content = ob_get_clean();
        echo $content;
    }

    /**
     * 渲染模板内容
     * @access public
     * @param string $template 模板名
     * @param array $vars 模板变量
     * @return void
     * @throws Exception
     */
    public function fetch($template, $vars = [])
    {
        $cacheFile = $this->config['cache_path'] . md5($template . Config::get('app_version')) . '.php';
        $this->checkCache($template, $cacheFile); // 缓存无效
        // 读取编译存储
        $this->cache->read($cacheFile, $vars);
    }

    /**
     * 检查编译缓存是否有效
     * 如果无效则需要重新编译
     * @access private
     * @param string $template 模板文件名
     * @param string $cacheFile 缓存文件名
     * @return void
     * @throws Exception
     */
    private function checkCache($template, $cacheFile)
    {
        if (APP_DEBUG || !$this->cache->check($cacheFile, $this->config['cache_time'])) {
            $content = file_get_contents($template);
            $content && $this->compiler($content, $cacheFile);
        }
    }

    /**
     * 编译模板文件内容
     * @access private
     * @param string $content 模板内容
     * @param string $cacheFile 缓存文件名
     * @return void
     * @throws Exception
     */
    private function compiler($content, $cacheFile)
    {
        // 模板解析
        $content = $this->parse($content);
        if ($this->config['strip_space']) {
            /* 去除html空格与换行 */
            $find = ['~>\s+<~', '~>(\s+\n|\r)~'];
            $replace = ['><', '>'];
            $content = preg_replace($find, $replace, $content);
        }
        // 优化生成的php代码
        $content = str_replace('?><?php', '', $content);
        // 编译存储
        $this->cache->write($cacheFile, $content);
        return;
    }

    /**
     * 模板解析入口
     * 支持普通标签和TagLib解析 支持自定义标签库
     * @access public
     * @param string $content 要解析的模板内容
     * @return string
     */
    public function parse($content)
    {
        // 检查include语法
        $content = $this->parseInclude($content);
        // 内置标签库 无需使用taglib标签导入就可以使用 并且不需使用标签库XML前缀
        $this->parseTagLib($content);
        // 解析普通模板标签 {tagName}
        return preg_replace_callback('/(' . $this->config['tpl_begin'] . ')([^' . $this->config['tpl_begin'] . $this->config['tpl_end'] . ']+)(' . $this->config['tpl_end'] . ')/is', function ($matches) {
            return $this->parseTag($matches[2], $matches[0]);
        }, $content);
    }

    // 解析模板中的include标签
    private function parseInclude($content)
    {
        // 解析继承
        $content = $this->parseExtend($content);
        // 读取模板中的include标签
        $find = preg_match_all('/' . $this->config['taglib_begin'] . 'include\s(.+?)[\s\/]?' . $this->config['taglib_end'] . '/is', $content, $matches);
        if ($find) {
            for ($i = 0; $i < $find; $i++) {
                $layout = explode('=', $matches[1][$i]);
                $content = str_replace($matches[0][$i], $this->parseIncludeItem(trim($layout[1], '"')), $content);
            }
        }
        return $content;
    }

    // 解析模板中的extend标签
    private function parseExtend($content)
    {
        $begin = $this->config['taglib_begin'];
        $end = $this->config['taglib_end'];
        // 读取模板中的继承标签
        $find = preg_match('/' . $begin . 'extend\s(.+?)[\s\/]?' . $end . '/is', $content, $matches);
        if ($find) {
            //替换extend标签
            $content = str_replace($matches[0], '', $content);
            // 记录页面中的block标签
            preg_replace_callback('/' . $begin . 'block\sname=(.+?)\s*?' . $end . '(.*?)' . $begin . '\/block' . $end . '/is', function ($matches) {
                return $this->parseBlock($matches[1], $matches[2]);
            }, $content);
            // 读取继承模板
            $layout = explode('=', $matches[1]);
            $content = $this->parseTemplateName(trim($layout[1], '"'));
            // 替换block标签
            $content = preg_replace_callback('/' . $begin . 'block\sname=(.+?)\s*?' . $end . '(.*?)' . $begin . '\/block' . $end . '/is', function ($matches) {
                return $this->replaceBlock($matches[1], $matches[2]);
            }, $content);
        } else {
            $content = preg_replace_callback('/' . $begin . 'block\sname=(.+?)\s*?' . $end . '(.*?)' . $begin . '\/block' . $end . '/is', function ($matches) {
                return stripslashes($matches[2]);
            }, $content);
        }
        return $content;
    }

    /**
     * 记录当前页面中的block标签
     * @access private
     * @param string $name block名称
     * @param string $content 模板内容
     * @return string
     */
    private function parseBlock($name, $content)
    {
        $this->block[$name] = $content;
        return '';
    }

    /**
     * 替换继承模板中的block标签
     * @access private
     * @param string $name block名称
     * @param string $content 模板内容
     * @return string
     */
    private function replaceBlock($name, $content)
    {
        // 替换block标签 没有重新定义则使用原来的
        $replace = isset($this->block[$name]) ? $this->block[$name] : $content;
        return stripslashes($replace);
    }

    /**
     * TagLib库解析
     * @access private
     * @param string $content 要解析的模板内容
     * @return void
     */
    protected function parseTagLib(&$content)
    {
        $begin = $this->config['taglib_begin'];
        $end = $this->config['taglib_end'];
        $tagObj = new Tag();
        $tagLib = $tagObj->getTags();
        foreach ($tagLib as $name => $val) {
            $tags = [$name];
            if (isset($val['alias'])) {// 别名设置
                $tags = explode(',', $val['alias']);
                $tags[] = $name;
            }
            $level = isset($val['level']) ? $val['level'] : 1;
            $closeTag = isset($val['close']) ? $val['close'] : true;
            foreach ($tags as $tag) {
                $parseTag = $tag;// 实际要解析的标签名称
                if (!method_exists($tagObj, '_' . $tag)) {
                    // 别名可以无需定义解析方法
                    $tag = $name;
                }
                $n1 = empty($val['attr']) ? '(\s*?)' : '\s([^' . $end . ']*)';
                if (!$closeTag) {
                    $patterns = '/' . $begin . $parseTag . $n1 . '\/(\s*?)' . $end . '/is';
                    $content = preg_replace_callback($patterns, function ($matches) use ($tagObj, $tag) {
                        return $this->parseXmlTag($tagObj, $tag, $matches[1], $matches[2]);
                    }, $content);
                } else {
                    $patterns = '/' . $begin . $parseTag . $n1 . $end . '(.*?)' . $begin . '\/' . $parseTag . '(\s*?)' . $end . '/is';
                    for ($i = 0; $i < $level; $i++) {
                        $content = preg_replace_callback($patterns, function ($matches) use ($tagObj, $tag) {
                            return $this->parseXmlTag($tagObj, $tag, $matches[1], $matches[2]);
                        }, $content);
                    }
                }
            }
        }
    }

    /**
     * 解析标签库的标签
     * 需要调用对应的标签库文件解析类
     * @access private
     * @param object $tagObj 模板引擎实例
     * @param string $tag 标签名
     * @param string $attr 标签属性
     * @param string $content 标签内容
     * @return string
     */
    private function parseXmlTag($tagObj, $tag, $attr, $content)
    {
        $attr = stripslashes($attr);
        $content = stripslashes($content);
        if (ini_get('magic_quotes_sybase')) {
            $attr = str_replace('\"', '\'', $attr);
        }
        $parse = '_' . $tag;
        $content = trim($content);
        $tags = $tagObj->parseXmlAttr($attr, $tag);
        return $tagObj->$parse($tags, $content);
    }

    /**
     * 模板标签解析
     * 格式： {TagName:args [|content] }
     * @access private
     * @param string $tagStr 标签内容
     * @param string $content 原始内容
     * @return string
     */
    private function parseTag($tagStr, $content)
    {
        $tagStr = stripslashes($tagStr);
        //还原非模板标签
        if (!preg_match('/^[\s|\d]/is', $tagStr)) {
            $flag = substr($tagStr, 0, 1);
            $flag2 = substr($tagStr, 1, 1);
            $name = substr($tagStr, 1);
            //解析模板变量 格式 {$varName}
            if ('$' == $flag && '.' != $flag2 && '(' != $flag2) {
                return $this->parseVar($name);
            } elseif ('-' == $flag || '+' == $flag) { // 输出计算
                return '<?php echo ' . $flag . $name . ';?>';
            } elseif (':' == $flag) { // 输出某个函数的结果
                return '<?php echo ' . $name . ';?>';
            } elseif ('~' == $flag) { // 执行某个函数
                return '<?php ' . $name . ';?>';
            } elseif (substr($tagStr, 0, 2) == '//' || (substr($tagStr, 0, 2) == '/*' && substr($tagStr, -2) == '*/')) {
                //注释标签
                return '';
            }
        }
        // 非法标签直接返回
        return $content;
    }

    /**
     * 模板变量解析,支持使用函数
     * 格式： {$varname|function1|function2=arg1,arg2}
     * @access private
     * @param string $varStr 变量数据
     * @return string
     */
    private function parseVar($varStr)
    {
        $varStr = trim($varStr);
        static $_varParseList = [];
        //如果已经解析过该变量字串，则直接返回变量值
        if (isset($_varParseList[$varStr])) {
            return $_varParseList[$varStr];
        }
        $parseStr = '';
        if (!empty($varStr)) {
            $varArray = explode('|', $varStr);
            //取得变量名称
            $var = array_shift($varArray);
            if (false !== strpos($var, '[')) {
                //支持 {$var['key']} 方式输出数组
                $name = "$" . $var;
            } elseif (false !== strpos($var, '.')) {
                //支持 {$var.property}
                $vars = explode('.', $var);
                $var = array_shift($vars);
                $name = '$' . $var;
                foreach ($vars as $key => $val) {
                    $name .= '["' . $val . '"]';
                }
            } elseif (false !== strpos($var, ':') && false === strpos($var, '(') && false === strpos($var, '::') && false === strpos($var, '?')) {
                //支持 {$var:property} 方式输出对象的属性
                $vars = explode(':', $var);
                $var = str_replace(':', '->', $var);
                $name = "$" . $var;
            } else {
                $name = "$$var";
            }
            //对变量使用函数
            if (count($varArray) > 0)
                $name = $this->parseVarFunction($name, $varArray);
            $parseStr = '<?php echo (' . $name . '); ?>';
        }
        $_varParseList[$varStr] = $parseStr;
        return $parseStr;
    }

    /**
     * 对模板变量使用函数
     * 格式 {$varname|function1|function2=arg1,arg2}
     * @access private
     * @param string $name 变量名
     * @param array $varArray 函数列表
     * @return string
     */
    private function parseVarFunction($name, $varArray)
    {
        //对变量使用函数
        $length = count($varArray);
        //取得模板禁止使用函数列表
        $template_deny_funs = explode(',', $this->config['tpl_deny_func_list']);
        for ($i = 0; $i < $length; $i++) {
            $args = explode('=', $varArray[$i], 2);
            //模板函数过滤
            $fun = strtolower(trim($args[0]));
            switch ($fun) {
                case 'default':  // 特殊模板函数
                    $name = '(' . $name . ')?(' . $name . '):' . $args[1];
                    break;
                default:  // 通用模板函数
                    if (!in_array($fun, $template_deny_funs)) {
                        if (isset($args[1])) {
                            if (strstr($args[1], '###')) {
                                $args[1] = str_replace('###', $name, $args[1]);
                                $name = "$fun($args[1])";
                            } else {
                                $name = "$fun($name,$args[1])";
                            }
                        } else if (!empty($args[0])) {
                            $name = "$fun($name)";
                        }
                    }
            }
        }
        return $name;
    }

    /**
     * 加载公共模板并缓存 和当前模板在同一路径，否则使用相对路径
     * @access private
     * @param string $tmplPublicName 公共模板文件名
     * @return string
     * @throws Exception
     */
    private function parseIncludeItem($tmplPublicName)
    {
        // 分析模板文件名并读取内容
        $parseStr = $this->parseTemplateName($tmplPublicName);
        // 再次对包含文件进行模板分析
        return $this->parseInclude($parseStr);
    }

    /**
     * 分析加载的模板文件并读取内容 支持多个模板文件读取
     * @access private
     * @param string $templateName 模板文件名
     * @return string
     * @throws Exception
     */
    private function parseTemplateName($templateName)
    {
        $array = explode(',', $templateName);
        $parseStr = '';
        foreach ($array as $templateName) {
            $template = $this->parseTemplateFile($templateName);
            // 获取模板文件内容
            $parseStr .= file_get_contents($template);
        }
        return $parseStr;
    }

    /**
     * 解析模板文件
     * @param $template
     * @return string
     * @throws Exception
     */
    private function parseTemplateFile($template)
    {
        $templateInfo = explode('/', $template);
        if (count($templateInfo) == 3) {
            $view = APP_BASE . $template . '.html';
        } else {
            if (empty($templateInfo[0])) {
                $template = CONTROLLER_NAME . '/' . ACTION_NAME;
            } elseif (empty($templateInfo[1])) {
                $template = CONTROLLER_NAME . '/' . $template;
            }
            $view = VIEW_PATH . $template . '.html';
        }
        if (is_file($view)) {
            return $view;
        } else {
            throw new Exception("模板文件{$view}不存在");
        }
    }

    /**
     * 字符串替换 避免正则混淆
     * @access private
     * @param string $str
     * @return mixed
     */
    private function stripPreg($str)
    {
        return str_replace(
            ['{', '}', '(', ')', '|', '[', ']', '-', '+', '*', '.', '^', '?'],
            ['\{', '\}', '\(', '\)', '\|', '\[', '\]', '\-', '\+', '\*', '\.', '\^', '\?'],
            $str);
    }
}
