<?php
namespace Craft\Kernel;

class Url
{
    /**
     * URL组装 支持不同URL模式
     * @param string $url URL表达式，格式：'[分组/模块/操作#锚点@域名]?参数1=值1&参数2=值2...'
     * @param string|array $vars 传入的参数，支持数组和字符串
     * @param string $anchor  锚点
     * @param array $queryArr
     * @param bool|string $domain 是否显示域名
     * @return string
     */
    public static function build($url = '', $vars = [], $queryArr = [], $anchor = '', $domain = '')
    {
        if ($url == '/') {
            return $url;
        }
        // 解析URL, 将url全部换成小写
        if (false !== strpos($url, '@')) { // 解析域名
            list($url, $host) = explode('@', $url, 2);
        }
        // 解析子域名
        if (!empty($host)) {
            $domain = $host . (strpos($host, '.') ? '' : strstr($_SERVER['HTTP_HOST'], '.'));
        }
        // 解析url
        $tmp = explode('/', $url);
        if (count($tmp) == 1) {
            $url = strtolower(CONTROLLER_NAME) . '/' . name_url_trans($url, 0);
        } else {
            $url = name_url_trans($tmp[0], 0) . '/' . name_url_trans($tmp[1], 0);
        }

        // 解析参数
        $sepr = Config::get('url_params.sepr');
        if (!empty($vars)) { // 添加参数
            $url .= '/' . implode($sepr, $vars);
        }
        if (!empty($queryArr)) {
            $url .= '?' . http_build_query($queryArr);
        }
        if (!empty($anchor)) {
            $url .= '#' . $anchor;
        }
        $url = '/' . $url;
        if ($domain) {
            $url = (self::is_ssl() ? 'https://' : 'http://') . $domain . $url;
        }
        return $url;
    }

    /**
     * 判断是否SSL协议
     * @return boolean
     */
    public static function is_ssl()
    {
        if (isset($_SERVER['HTTPS']) && ('1' == $_SERVER['HTTPS'] || 'on' == strtolower($_SERVER['HTTPS']))) {
            return true;
        } elseif (isset($_SERVER['SERVER_PORT']) && ('443' == $_SERVER['SERVER_PORT'])) {
            return true;
        }
        return false;
    }
}
