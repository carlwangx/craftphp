<?php
/**
 * 视图层
 */
namespace Craft\Kernel;

use Craft\Kernel\View\Template;

class View
{
    protected $engine = null; // 模板引擎实例
    protected $data = [];   // 模板变量

    public function __construct()
    {
        $this->engine = new Template(); // 视图解析引擎
    }

    /**
     * 模板变量赋值
     * @param $name
     * @param string $value
     * @return void
     */
    public function assign($name, $value = '')
    {
        if (is_array($name)) {
            $this->data = array_merge($this->data, $name);
        } else {
            $this->data[$name] = $value;
        }
    }

    /**
     * 加载模板和页面输出 可以返回输出内容
     * @access public
     * @param string $template 模板文件名
     * @return mixed
     */
    public function display($template = '')
    {
        // 解析并获取模板内容
        $content = $this->fetch($template);
        // 输出模板内容
        header('Content-Type:text/html; charset=utf-8');
        header('Cache-control: private');  // 页面缓存控制
        header('X-Powered-By: CraftX');
        // 输出模板文件
        echo $content;
    }

    /**
     * 解析和获取模板内容 用于输出
     * @access public
     * @param string $template 模板文件名或者内容
     * @return string
     */
    public function fetch($template)
    {
        // 页面缓存
        ob_start();
        ob_implicit_flush(0);
        $this->engine->display($template, $this->data);
        // 获取并清空缓存
        return ob_get_clean();
    }
}
