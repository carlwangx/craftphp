<?php

namespace Craft\Kernel;

class Cli
{
    /**
     * URL调度，反射机制
     * Exception:
     * 1 控制器不存在
     * 2 方法不存在
     * @param $argv
     * @throws \ReflectionException
     * @throws \Exception
     */
    public static function start($argv)
    {
        define('CONTROLLER_NAME', ucfirst($argv[1]));
        $action = $argv[2] ?? 'index';
        define('ACTION_NAME', $action);
        $instance = Loader::controller(CONTROLLER_NAME);
        if (method_exists($instance, $action)) {
            //执行当前操作
            $method = new \ReflectionMethod($instance, $action);
            if ($method->isPublic()) {
                $method->invoke($instance);
            } else {
                // 操作方法不是Public 抛出异常
                throw new \ReflectionException($action . '必须是public');
            }
        } else {
            // 方法不存在
            throw new \ReflectionException($action . '方法不存在');
        }
    }
}