<?php

namespace Craft\Kernel\Error;

class Cli extends Base
{
    /**
     * 程序终止
     */
    public static function shutdown()
    {
        static::beforeShutdown();
    }
}