<?php
namespace Craft\Kernel\Error;

class Api extends Base
{
    /**
     * 程序终止
     */
    public static function shutdown()
    {
        static::beforeShutdown();
        if (static::$hasError) {
            $data = [
                'code' => 500,
                'msg' => '系统错误',
                'data' => []
            ];
            $data = json_encode($data);
            exit($data);
        }
    }
}