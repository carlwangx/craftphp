<?php
namespace Craft\Kernel\Error;

use Craft\Kernel\Runtime\Log;
use Craft\Kernel\Runtime\Notice;
use Craft\Kernel\Runtime\Trace;

abstract class Base
{
    protected static $hasError = false;

    abstract public static function shutdown();

    /**
     * 处理 Throwable
     * @param $e \Throwable
     */
    public static function exception($e)
    {
        static::$hasError = true;
        $error = [];
        $error['message'] = $e->getMessage();
        $trace = $e->getTrace();
        if ('E' == $trace[0]['function']) {
            $error['file'] = $trace[0]['file'];
            $error['line'] = $trace[0]['line'];
        } else {
            $error['file'] = $e->getFile();
            $error['line'] = $e->getLine();
        }
        $error['trace'] = $e->getTraceAsString();
        // 记录异常日志
        Log::record($error['message'], 'ERROR');
        Log::record($error['trace'], 'ERROR');
    }

    /**
     * 处理E_WARNING, E_NOTICE, trigger_error()
     * @param $errno 错误的级别
     * @param $errstr 错误的信息
     * @param $errfile 错误的文件名
     * @param $errline 错误发生的行号
     */
    public static function error($errno, $errstr, $errfile, $errline)
    {
        $errorStr = "[{$errno}] {$errstr} {$errfile} 第 {$errline} 行.";
        switch ($errno) {
            case E_USER_ERROR:
                Log::record($errorStr, 'ERR');
                static::$hasError = true;
                break;
            case E_STRICT:
            case E_USER_WARNING:
            case E_USER_NOTICE:
            default:
                Log::record($errorStr, 'NOTIC');
                break;
        }
    }

    /**
     * 程序终止
     * @return bool
     */
    public static function beforeShutdown()
    {
        // 记录日志
        Trace::save();
        // 获取最后的错误信息, 以该错误的 "type"、 "message"、"file" 和 "line" 为数组的键
        $error = error_get_last();
        if ($error) {
            // 邮箱错误警告
            $errorNotice = C('notice');
            if (!empty($errorNotice['mail']) && !empty($errorNotice['mail']['send'])) {
                Notice::email($errorNotice['mail']);
            }
        }
    }

    /**
     * 确定错误类型是否致命
     *
     * @param  int $type
     * @return bool
     */
    protected static function isFatal($type)
    {
        return in_array($type, [E_ERROR, E_CORE_ERROR, E_COMPILE_ERROR, E_PARSE]);
    }
}