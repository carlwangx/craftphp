<?php

namespace Craft\Kernel\Error;

class Web extends Base
{
    /**
     * 程序终止
     */
    public static function shutdown()
    {
        static::beforeShutdown();
        if (static::$hasError) {
            redirect(U(C('error_page')), true);
        }
    }
}