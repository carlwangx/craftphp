<?php

namespace Craft\Kernel\Trace;

use Craft\Factory\Model;

/**
 * 本地化调试输出到数据库
 */
class Db
{
    protected $storage;

    public function __construct()
    {
        $this->storage = Model::instance();
    }

    /**
     * 日志写入接口
     * @access public
     * @param array $log 日志信息
     * @return void
     */
    public function save(array $log = [])
    {
        $info = '';
        foreach ($log as $type => $val) {
            $level = '';
            foreach ($val as $msg) {
                if (!is_string($msg)) {
                    $msg = var_export($msg, true);
                }
                $level .= '[ ' . $type . ' ] ' . $msg . "\r\n";
            }
            if (in_array($type, $this->config['apart_level'])) {
                // 独立记录的日志级别
                $filename = $this->config['path'] . '/' . date('d') . '_' . $type . '.log';
                $this->write($level, $filename, true);
            } else {
                $info .= $level;
            }
        }
        if ($info) {
            $this->storage->insert([], 'log');
        }
    }
}
