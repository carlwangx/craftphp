<?php
/**
 * 处理日记记录
 */
namespace Craft\Kernel\Runtime;

class Log
{
    const ERROR  = 'error';
    const INFO   = 'info';
    const SQL    = 'sql';
    const NOTICE = 'notice';
    const DEBUG  = 'debug';

    // 日志信息
    protected static $log = [];

    // 日志类型
    protected static $type = ['error', 'info', 'sql', 'notice', 'debug'];

    /**
     * 记录日志 并且会过滤未经设置的级别
     * @access public
     * @param string $message 日志信息
     * @param string $type 日志类型
     * @return void
     */
    public static function record($message, $type = 'info')
    {
        self::$log[$type][] = "{$type}: {$message}";
    }

    /**
     * 获取内存中的日志信息
     * @access public
     * @param string $type 日志类型
     * @return array
     */
    public static function getLog($type = '')
    {
        return $type ? self::$log[$type] : self::$log;
    }
}