<?php
namespace Craft\Kernel\Runtime;

use Craft\Service\Mail;

/**
 * 运行错误通知
 * Class Notice
 * @package Craft\Kernel\Runtime
 */
class Notice
{
    /**
     * @param $config
     * @throws \Exception
     * @throws \phpmailerException
     */
    public static function email($config)
    {
        $logs = Log::getLog($config['level']);
        $logsHtml = '';
        if ($logs) {
            $MailServer = Mail::getInstance($config['name']);
            foreach ($config['receiver'] as $email) {
                $MailServer->addAddress($email);
            }
            $MailServer->Subject = $config['subject'] . $_SERVER['HTTP_HOST'];
            foreach ($_SERVER as $k => $v) {
                $logsHtml .= "<p>{$k}:{$v}</p>";
            }
            $logsHtml .= '<table><tr><th>错误级别</th><th>具体错误</th></tr>';
            foreach ($logs as $type => $items) {
                $logsHtml .= "<tr><td>{$type}</td><td>";
                foreach ($items as $item) {
                    $logsHtml .= $item . '<br>';
                }
                $logsHtml .= '</td></tr>';
            }
            $logsHtml .= '</table>';
            $MailServer->msgHTML($logsHtml);
            $MailServer->send();
        }
    }
}