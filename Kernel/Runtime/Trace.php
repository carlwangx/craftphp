<?php
namespace Craft\Kernel\Runtime;

use Craft\Kernel\Config;
use Craft\Kernel\Trace\Db;
use Craft\Kernel\Trace\File;

class Trace
{
    public static function get()
    {
        // 系统默认显示信息
        $files = get_included_files();
        $info = [];
        foreach ($files as $key => $file) {
            $info[] = $file . ' ( ' . number_format(filesize($file) / 1024, 2) . ' KB )';
        }
        $requestInfo = date('Y-m-d H:i:s', $_SERVER['REQUEST_TIME']) . ' ' . $_SERVER['SERVER_PROTOCOL'] . ' ' . $_SERVER['REQUEST_METHOD'] . ' : ' . $_SERVER['PHP_SELF'];
        $trace['base'] = [
            'request' => $requestInfo,
            'time' => round(microtime(true) - START_TIME, 10),
            'memory' => number_format((memory_get_usage() - START_MEM) / 1024, 2),
            'load_file' => count($files),
            'config' => count(Config::get()),
        ];
        $log = Log::getLog();
        array_merge($trace, $log);
        return $trace;
    }

    /**
     * 日志保存
     * @access public
     * @return void
     */
    public static function save()
    {
        $log = self::get();
        $traceConf = C('trace');
        if ($traceConf['enable']) {
            if ($traceConf['driver'] == 'file') {
                $handler = new File();
            } else {
                $handler = new Db();
            }
            $handler->save($log);
        } elseif (APP_DEBUG) {
            var_dump($log);
        }
    }
}
