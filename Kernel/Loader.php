<?php
namespace Craft\Kernel;

class Loader
{
    // 类名映射
    protected static $map = [];
    // 命名空间
    protected static $namespace = [];

    // 注册自动加载机制
    public static function register()
    {
        spl_autoload_register(['Craft\Kernel\Loader', 'autoload']);
    }

    // 自动加载
    public static function autoload($class)
    {
        // 检查是否定义classmap
        if (isset(self::$map[$class])) {
            include self::$map[$class];
        } else { // 命名空间自动加载
            $name = strstr($class, '\\', true);
            if ($name == 'Craft') {
                $class = ltrim($class, $name . '\\');
            } else {
                $name = 'App';
            }
            $path = self::$namespace[$name];
            $filename = $path . str_replace('\\', '/', $class) . '.php';
            if (is_file($filename)) {
                self::$map[$class] = $filename;
                include $filename;
            }
        }
    }

    /**
     * 注册类映射关系
     * @param $class
     */
    public static function addClassMap($class)
    {
        self::$map = array_merge(self::$map, $class);
    }

    /**
     * 注册命名空间
     * @param $namespace
     */
    public static function addNamespaceMap($namespace)
    {
        self::$namespace = array_merge(self::$namespace, $namespace);
    }

    /**
     * 实例化（分层）控制器 格式：[模块名/]控制器名
     * @param string $name 资源地址
     * @return false|Object
     * @throws \Exception
     */
    public static function controller($name)
    {
        if (defined('APP_NAME')) {
            $class = 'App\\' . APP_NAME . '\\' . MODULE_NAME . '\\Controller\\' . $name;
        } else {
            $class = 'App\\' . MODULE_NAME . '\\Controller\\' . $name;
        }
        if (class_exists($class)) {
            $action = new $class;
            return $action;
        } else {
            throw new \Exception('实例化不存在的类：' . $class);
        }
    }
}
