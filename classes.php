<?php
return [
    'namespace' => [
        'Craft' => FRAMEWORK_ROOT,
        'App' => PROJECT_ROOT
    ],
    'class' => [
        'Craft\Kernel\Monitor'                 =>  FRAMEWORK_ROOT . 'Kernel/Monitor.php',
        'Craft\Kernel\Log'                     =>  FRAMEWORK_ROOT . 'Kernel/Log.php',
        'Craft\Kernel\Log\Driver\File'         =>  FRAMEWORK_ROOT . 'Kernel/Log/Driver/File.php',
        'Craft\Kernel\Storage\Db'              =>  FRAMEWORK_ROOT . 'Kernel/Storage/Db.php',
        'Craft\Kernel\Route'                   =>  FRAMEWORK_ROOT . 'Kernel/Route.php',
        'Craft\Kernel\Exception'               =>  FRAMEWORK_ROOT . 'Kernel/Exception.php',
        'Craft\Kernel\Model'                   =>  FRAMEWORK_ROOT . 'Kernel/Model.php',
        'Craft\Kernel\Db\Driver'               =>  FRAMEWORK_ROOT . 'Kernel/Db/Driver.php',
        'Craft\Kernel\Template'                =>  FRAMEWORK_ROOT . 'Kernel/Template.php',
        'Craft\Kernel\Template\Driver\File'    =>  FRAMEWORK_ROOT . 'Kernel/Template/Driver/File.php',
        'Craft\Kernel\Error'                   =>  FRAMEWORK_ROOT . 'Kernel/Error.php',
        'Craft\Kernel\Cache'                   =>  FRAMEWORK_ROOT . 'Kernel/Cache.php',
        'Craft\Kernel\Storage\Driver\Redis'    =>  FRAMEWORK_ROOT . 'Kernel/Storage/Driver/Redis.php',
        'Craft\Kernel\Storage\Driver\Db'       =>  FRAMEWORK_ROOT . 'Kernel/Storage/Driver/Db.php',
        'Craft\Kernel\Storage\Driver\Mysql'    =>  FRAMEWORK_ROOT . 'Kernel/Storage/Driver/Mysql.php',
        'Craft\Kernel\Session'                 =>  FRAMEWORK_ROOT . 'Kernel/Session.php',
        'Craft\Kernel\Cookie'                  =>  FRAMEWORK_ROOT . 'Kernel/Cookie.php',
        'Craft\Kernel\Controller'              =>  FRAMEWORK_ROOT . 'Kernel/Controller.php',
        'Craft\Kernel\View'                    =>  FRAMEWORK_ROOT . 'Kernel/View.php',
        'Craft\Kernel\Url'                     =>  FRAMEWORK_ROOT . 'Kernel/Url.php',
        'Craft\Kernel\Input'                   =>  FRAMEWORK_ROOT . 'Kernel/Input.php',
        'Craft\Kernel\Parser'                  =>  FRAMEWORK_ROOT . 'Kernel/Parser.php',
    ],
];