<?php


namespace Craft\Util;

use \Firebase\JWT\JWT;


class JwtAuth
{
    protected $algs = ['HS256'];

    protected function getSecretKey()
    {
        return C('JWT_SECRET');
    }

    /**
     * 生成token
     *
     * @param string $requestUrl 当前请求的url
     * @param array $payload
     * @param int $timeout
     * @return string
     */
    public function encrypt($requestUrl, array $payload, $timeout = 0)
    {

        $secret = $this->getSecretKey();
        $claims = [
            'iss' => $requestUrl,
        ];

        if ($timeout > 0) {
            $claims['exp'] = time() + $timeout;
        }
        foreach ($payload as $key => $value) {
            $claims[$key] = $value;
        }

        return JWT::encode($claims, $secret);
    }


    /**
     * 验证token, 成功返回payload数据对象, 无效或过期则抛出异常
     * @param $token
     * @return array
     */
    public function verify($token)
    {
        $secret = $this->getSecretKey();
        return (array)JWT::decode($token, $secret, $this->algs);
    }


    /**
     * 获取命名,支持cookie和header获取
     *
     * @param \Illuminate\Http\Request $request
     * @param string $appTokenName
     * @return array|mixed
     * @throws ConfigException
     */
    public function getSignature($request, $appTokenName = '')
    {
        $cookieName = $appTokenName == '' ? env('APP_TOKEN_NAME', '') : $appTokenName;
        if (!$cookieName) {
            throw new ConfigException('APP_TOKEN_NAME');
        }

        $token = $request->cookies->get($cookieName, '');
        if (!$token) {
            $authHeader = $request->headers->get('Authorization');
            if ($authHeader !== null && preg_match('/^Bearer\s+(.*?)$/', $authHeader, $matches)) {
                $token = $matches[1];
            }
        }
        return $token;
    }

}