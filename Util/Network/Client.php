<?php
/**
 * 获取客户端信息
 * @author carl.wang<2009wpeng@gmail.com>
 * @date: 2015-06-24
 * @version 0.0.1
 */
namespace Craft\Kernel\Network;

class Client
{
    /**
     * 获取客户端IP地址
     * @param integer $type 返回类型 0 返回IP地址 1 返回IPV4地址数字
     * @param boolean $adv 是否进行高级模式获取（有可能被伪装）
     * @return mixed
     */
    public function getIp($type = 0, $adv = true)
    {
        $type = $type ? 1 : 0;
        static $ip = NULL;
        if ($ip !== NULL) return $ip[$type];
        if ($adv) {
            if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                $arr = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
                $pos = array_search('unknown', $arr);
                if (false !== $pos) unset($arr[$pos]);
                $ip = trim($arr[0]);
            } elseif (isset($_SERVER['HTTP_CLIENT_IP'])) {
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            } elseif (isset($_SERVER['REMOTE_ADDR'])) {
                $ip = $_SERVER['REMOTE_ADDR'];
            }
        } elseif (isset($_SERVER['REMOTE_ADDR'])) {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        // IP地址合法验证
        $long = sprintf("%u", ip2long($ip));
        $ip = $long ? array($ip, $long) : array('0.0.0.0', 0);
        return $ip[$type];
    }

    public function getLocation($ip = '')
    {
        if (empty($ip)) {
            $ip = $this->getIp();
        }
        $taobaoUrl = "http://ip.taobao.com/service/getIpInfo.php?ip=";
        $url       = $taobaoUrl . $ip;
        $data      = @file_get_contents($url);
        return json_decode($data, true);
    }

    /**
     * 获取客户端信息
     */
    public function getOs()
    {
        $os = 'unknown';
        $agent = $_SERVER['HTTP_USER_AGENT'];
        if (preg_match('/win/i', $agent)) {
            $os = 'Windows';
        } elseif (preg_match('/mac/i',$agent)) {
            $os = 'mac';
        } elseif (preg_match('/linux/i',$agent)) {
            $OS = 'Linux';
        }
        elseif (preg_match('/unix/i',$agent)) {
            $OS = 'Unix';
        }
        elseif (preg_match('/bsd/i',$agent)) {
            $OS = 'BSD';
        }
    }


    public function getBrowser()
    {
        $agent = $_SERVER['HTTP_USER_AGENT'];
        if (preg_match('/MSIE\s([^\s|;]+)/i', $agent, $regs)) {
            $browser  = 'Internet Explorer';
            $browser_ver   = $regs[1];
        }

    }

    public function getLanguage()
    {
        //只取前4位，这样只判断最优先的语言。如果取前5位，可能出现en,zh的情况，影响判断。
        $lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 4);
        if (preg_match("/zh-c/i", $lang)) {
            $language = 'Simplified';
        } elseif (preg_match("/zh/i", $lang)) {
            $language = 'Traditional';
        } elseif (preg_match("/en/i", $lang)) {
            $language = 'English';
        } elseif (preg_match("/fr/i", $lang)) {
            $language = 'French';
        } elseif (preg_match("/de/i", $lang)) {
            $language = 'German';
        } elseif (preg_match("/jp/i", $lang)) {
            $language = 'Japanese';
        } elseif (preg_match("/ko/i", $lang)) {
            $language = 'Korean';
        } elseif (preg_match("/es/i", $lang)) {
            $language = 'Spanish';
        } elseif (preg_match("/sv/i", $lang)) {
            $language = 'Swedish';
        } else {
            // 默认英文
            $language = 'English';
        }
        return $language;
    }


}