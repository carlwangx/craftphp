<?php
/**
 * 根据IP进行访问控制
 * @author carl.wang<2009wpeng@gmail.com>
 * @date: 2015-06-18
 * @version 0.0.1
 */
namespace Craft\Kernel\Security;

class Ip
{
    
    /**
     * 禁止访问的IP
     * @return bool
     */
    public function block()
    {
        $blockIps = C('block_ips');
        $ip = get_client_ip();
        if (in_array($ip, $blockIps)) {
            return false;
        }
        return true;
    }

    /**
     * 允许访问的IP
     * @return bool
     */
    public function allow()
    {
        $allowIps = C('allow_ips');
        $ip = get_client_ip();
        if (in_array($ip, $allowIps)) {
            return true;
        }
        return false;
    }
}