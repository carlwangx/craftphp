<?php

namespace Craft\Util;

class Markdown
{
    protected $breaksEnabled;

    protected $markupEscaped;

    private static $instances = [];

    protected $definitionData;

    protected $specialCharacters = ['\\', '`', '*', '_', '{', '}', '[', ']', '(', ')', '>', '#', '+', '-', '.', '!', '|'];

    protected $strongRegex = [
        '*' => '/^[*]{2}((?:\\\\\*|[^*]|[*][^*]*[*])+?)[*]{2}(?![*])/s',
        '_' => '/^__((?:\\\\_|[^_]|_[^_]*_)+?)__(?!_)/us',
    ];

    protected $emRegex = [
        '*' => '/^[*]((?:\\\\\*|[^*]|[*][*][^*]+?[*][*])+?)[*](?![*])/s',
        '_' => '/^_((?:\\\\_|[^_]|__[^_]*__)+?)_(?!_)\b/us',
    ];

    protected $regexHtmlAttribute = '[a-zA-Z_:][\w:.-]*(?:\s*=\s*(?:[^"\'=<>`\s]+|"[^"]*"|\'[^\']*\'))?';

    protected $voidElements = [
        'area', 'base', 'br', 'col', 'command', 'embed', 'hr', 'img', 'input', 'link', 'meta', 'param', 'source',
    ];

    protected $textLevelElements = [
        'a', 'br', 'bdo', 'abbr', 'blink', 'nextid', 'acronym', 'basefont',
        'b', 'em', 'big', 'cite', 'small', 'spacer', 'listing',
        'i', 'rp', 'del', 'code', 'strike', 'marquee',
        'q', 'rt', 'ins', 'font', 'strong',
        's', 'tt', 'kbd', 'mark',
        'u', 'xm', 'sub', 'nobr',
        'sup', 'ruby',
        'var', 'span',
        'wbr', 'time',
    ];

    protected $InlineTypes = [
        '"' => array('SpecialCharacter'),
        '!' => array('Image'),
        '&' => array('SpecialCharacter'),
        '*' => array('Emphasis'),
        ':' => array('Url'),
        '<' => array('UrlTag', 'EmailTag', 'Markup', 'SpecialCharacter'),
        '>' => array('SpecialCharacter'),
        '[' => array('Link'),
        '_' => array('Emphasis'),
        '`' => array('Code'),
        '~' => array('Strikethrough'),
        '\\' => array('EscapeSequence'),
    ];

    // 行内的标记
    protected $inlineMarkerList = '!"*_&[:<>`~\\';

    //
    protected $blockTypes = [
        '#' => array('Header'),
        '*' => array('Rule', 'List'),
        '+' => array('List'),
        '-' => array('SetextHeader', 'Table', 'Rule', 'List'),
        '0' => array('List'),
        '1' => array('List'),
        '2' => array('List'),
        '3' => array('List'),
        '4' => array('List'),
        '5' => array('List'),
        '6' => array('List'),
        '7' => array('List'),
        '8' => array('List'),
        '9' => array('List'),
        ':' => array('Table'),
        '<' => array('Comment', 'Markup'),
        '=' => array('SetextHeader'),
        '>' => array('Quote'),
        '[' => array('Reference'),
        '_' => array('Rule'),
        '`' => array('FencedCode'),
        '|' => array('Table'),
        '~' => array('FencedCode'),
    ];

    protected $urlsLinked = true;

    protected $unmarkedBlockTypes = ['Code'];

    /**
     * @param string $name
     * @return mixed
     */
    public static function instance($name = 'default')
    {
        if (!isset(self::$instances[$name])) {
            self::$instances[$name] = new static();
        }
        return self::$instances[$name];
    }

    public function setBreaksEnabled($breaksEnabled)
    {
        $this->breaksEnabled = $breaksEnabled;
        return $this;
    }


    public function setMarkupEscaped($markupEscaped)
    {
        $this->markupEscaped = $markupEscaped;
        return $this;
    }

    public function setUrlsLinked($urlsLinked)
    {
        $this->urlsLinked = $urlsLinked;
        return $this;
    }

    public function parse($text)
    {
        # make sure no definitions are set
        $this->definitionData = [];
        # standardize line breaks
        $text = str_replace(["\r\n", "\r"], "\n", $text);
        # remove surrounding line breaks
        $text = trim($text, "\n");
        # split text into lines
        $lines = explode("\n", $text);
        # iterate through lines to identify blocks
        $markup = $this->lines($lines);
        # trim line breaks
        return trim($markup, "\n");
    }

    protected function lines(array $lines)
    {
        $currentBlock = null;
        foreach ($lines as $line) {
            if (rtrim($line) === '') {
                if (isset($currentBlock)) {
                    $currentBlock['interrupted'] = true;
                }
                continue;
            }
            // \t 制表符解析
            if (strpos($line, "\t") !== false) {
                $parts = explode("\t", $line);
                $line = $parts[0];
                unset($parts[0]);
                foreach ($parts as $part) {
                    $shortage = 4 - mb_strlen($line, 'utf-8') % 4;
                    $line .= str_repeat(' ', $shortage);
                    $line .= $part;
                }
            }
            $indent = 0;
            while (isset($line[$indent]) && $line[$indent] === ' ') {
                $indent++;
            }
            $text = $indent > 0 ? substr($line, $indent) : $line;
            $lineInfo = ['body' => $line, 'indent' => $indent, 'text' => $text];
            if (isset($currentBlock['continuable'])) {
                $blockInfo = $this->{'block' . $currentBlock['type'] . 'Continue'}($lineInfo, $currentBlock);
                if (isset($blockInfo)) {
                    $currentBlock = $blockInfo;
                    continue;
                } else {
                    if ($this->isBlockCompletable($currentBlock['type'])) {
                        $currentBlock = $this->{'block' . $currentBlock['type'] . 'Complete'}($currentBlock);
                    }
                }
            }
            $marker = $text[0];
            $blockTypes = $this->unmarkedBlockTypes;
            isset($this->blockTypes[$marker]) && $blockTypes = array_merge($blockTypes, $this->blockTypes[$marker]);
            foreach ($blockTypes as $blockType) {
                // 获取block信息
                $blockInfo = $this->{'block' . $blockType}($lineInfo);
                if ($blockInfo) {
                    $blockInfo['type'] = $blockType;
                    if (!isset($blockInfo['identified'])) {
                        $blockInfo[] = $currentBlock;
                        $blockInfo['identified'] = true;
                    }
                    if ($this->isBlockContinuable($blockType)) {
                        $blockInfo['continuable'] = true;
                    }
                    $currentBlock = $blockInfo;
                    // 跳出整个循环
                    continue 2;
                }
            }
            if (isset($currentBlock) && !isset($currentBlock['type']) && !isset($currentBlock['interrupted'])) {
                $currentBlock['element']['text'] .= "\n" . $text;
            } else {
                $Blocks[] = $currentBlock;
                $currentBlock = [
                    'element' => [
                        'name' => 'p',
                        'text' => $lineInfo['text'],
                        'handler' => 'line',
                    ],
                    'identified' => true
                ];
            }
        }
        //
        if (isset($currentBlock['continuable']) && $this->isBlockCompletable($currentBlock['type'])) {
            $currentBlock = $this->{'block' . $currentBlock['type'] . 'Complete'}($currentBlock);
        }
        $Blocks[] = $currentBlock;
        unset($Blocks[0]);
        $markup = '';
        foreach ($Blocks as $Block) {
            if (isset($Block['hidden'])) {
                continue;
            }
            $markup .= "\n";
            $markup .= isset($Block['markup']) ? $Block['markup'] : $this->element($Block['element']);
        }
        return $markup . "\n";
    }

    /**
     * 解析元素
     * @param array $element
     * @return string
     */
    protected function element(array $element)
    {
        $markup = '<' . $element['name'];
        // 属性
        if (!empty($element['attributes'])) {
            foreach ($element['attributes'] as $name => $value) {
                if ($value) {
                    $markup .= ' ' . $name . '="' . $value . '"';
                }
            }
        }
        if (!empty($element['text'])) {
            $markup .= '>';
            if (isset($element['handler'])) {
                $markup .= $this->{$element['handler']}($element['text']);
            } else {
                $markup .= $element['text'];
            }
            $markup .= '</' . $element['name'] . '>';
        } else {
            $markup .= ' />';
        }
        return $markup;
    }

    private function isBlockContinuable($type)
    {
        return method_exists($this, 'block' . $type . 'Continue');
    }

    private function isBlockCompletable($type)
    {
        return method_exists($this, 'block' . $type . 'Complete');
    }

    /**
     * @param $lineInfo
     * @param null $Block
     * @return array|bool|null
     */
    protected function blockCode($lineInfo, $Block = null)
    {
        if (isset($Block) && !isset($Block['type']) && !isset($Block['interrupted'])) {
            return;
        }
        if ($lineInfo['indent'] >= 4) {
            $text = substr($lineInfo['body'], 4);
            $Block = [
                'element' => [
                    'name' => 'pre',
                    'handler' => 'element',
                    'text' => [
                        'name' => 'code',
                        'text' => $text,
                    ],
                ],
            ];
            return $Block;
        }
    }

    protected function blockCodeContinue($lineInfo, $blockInfo)
    {
        if ($lineInfo['indent'] >= 4) {
            if (isset($blockInfo['interrupted'])) {
                $blockInfo['element']['text']['text'] .= "\n";
                unset($blockInfo['interrupted']);
            }
            $blockInfo['element']['text']['text'] .= "\n";
            $text = substr($lineInfo['body'], 4);
            $blockInfo['element']['text']['text'] .= $text;
            return $blockInfo;
        }
    }

    protected function blockCodeComplete($Block)
    {
        $text = $Block['element']['text']['text'];
        $text = htmlspecialchars($text, ENT_NOQUOTES, 'UTF-8');
        $Block['element']['text']['text'] = $text;
        return $Block;
    }

    protected function blockComment($Line)
    {
        if ($this->markupEscaped) {
            return;
        }
        if (isset($Line['text'][3]) && $Line['text'][3] === '-' && $Line['text'][2] === '-' && $Line['text'][1] === '!') {
            $Block = [
                'markup' => $Line['body'],
            ];
            if (preg_match('/-->$/', $Line['text'])) {
                $Block['closed'] = true;
            }
            return $Block;
        }
    }

    protected function blockCommentContinue($Line, array $Block)
    {
        if (isset($Block['closed'])) {
            return;
        }
        $Block['markup'] .= "\n" . $Line['body'];
        if (preg_match('/-->$/', $Line['text'])) {
            $Block['closed'] = true;
        }
        return $Block;
    }

    protected function blockFencedCode($Line)
    {
        if (preg_match('/^[' . $Line['text'][0] . ']{3,}[ ]*([\w-]+)?[ ]*$/', $Line['text'], $matches)) {
            $Element = [
                'name' => 'code',
                'text' => '',
            ];
            if (isset($matches[1])) {
                $class = 'language-' . $matches[1];
                $Element['attributes'] = [
                    'class' => $class,
                ];
            }
            $Block = [
                'char' => $Line['text'][0],
                'element' => [
                    'name' => 'pre',
                    'handler' => 'element',
                    'text' => $Element,
                ],
            ];
            return $Block;
        }
    }

    protected function blockFencedCodeContinue($Line, $Block)
    {
        if (isset($Block['complete'])) {
            return;
        }
        if (isset($Block['interrupted'])) {
            $Block['element']['text']['text'] .= "\n";
            unset($Block['interrupted']);
        }
        if (preg_match('/^' . $Block['char'] . '{3,}[ ]*$/', $Line['text'])) {
            $Block['element']['text']['text'] = substr($Block['element']['text']['text'], 1);

            $Block['complete'] = true;

            return $Block;
        }
        $Block['element']['text']['text'] .= "\n" . $Line['body'];
        return $Block;
    }

    protected function blockFencedCodeComplete($Block)
    {
        $text = $Block['element']['text']['text'];

        $text = htmlspecialchars($text, ENT_NOQUOTES, 'UTF-8');

        $Block['element']['text']['text'] = $text;

        return $Block;
    }

    protected function blockHeader($lineInfo)
    {
        $text = ltrim($lineInfo['text'], '#');
        $level = strlen($lineInfo['text']) - strlen($text); // #的个数
        if ($level <= 6) {
            return [
                'element' => [
                    'name' => 'h' . $level,
                    'text' => $text,
                    'handler' => 'line',
                ],
            ];
        }
    }

    protected function blockList($Line)
    {
        list($name, $pattern) = $Line['text'][0] <= '-' ? ['ul', '[*+-]'] : ['ol', '[0-9]+[.]'];
        if (preg_match('/^(' . $pattern . '[ ]+)(.*)/', $Line['text'], $matches)) {
            $Block = [
                'indent' => $Line['indent'],
                'pattern' => $pattern,
                'element' => [
                    'name' => $name,
                    'handler' => 'elements',
                ],
            ];
            if ($name === 'ol') {
                $listStart = stristr($matches[0], '.', true);

                if ($listStart !== '1') {
                    $Block['element']['attributes'] = ['start' => $listStart];
                }
            }
            $Block['li'] = [
                'name' => 'li',
                'handler' => 'li',
                'text' => [
                    $matches[2],
                ],
            ];
            $Block['element']['text'][] = &$Block['li'];
            return $Block;
        }
    }

    protected function blockListContinue($Line, array $Block)
    {
        if ($Block['indent'] === $Line['indent'] && preg_match('/^' . $Block['pattern'] . '(?:[ ]+(.*)|$)/', $Line['text'], $matches)) {
            if (isset($Block['interrupted'])) {
                $Block['li']['text'][] = '';
                unset($Block['interrupted']);
            }
            unset($Block['li']);
            $text = isset($matches[1]) ? $matches[1] : '';
            $Block['li'] = [
                'name' => 'li',
                'handler' => 'li',
                'text' => [
                    $text,
                ],
            ];
            $Block['element']['text'] [] = &$Block['li'];
            return $Block;
        }
        if ($Line['text'][0] === '[' && $this->blockReference($Line)) {
            return $Block;
        }
        if (!isset($Block['interrupted'])) {
            $text = preg_replace('/^[ ]{0,4}/', '', $Line['body']);
            $Block['li']['text'] [] = $text;
            return $Block;
        }
        if ($Line['indent'] > 0) {
            $Block['li']['text'] [] = '';
            $text = preg_replace('/^[ ]{0,4}/', '', $Line['body']);
            $Block['li']['text'] [] = $text;
            unset($Block['interrupted']);
            return $Block;
        }
    }

    protected function blockQuote($lineInfo)
    {
        if (preg_match('/^>[ ]?(.*)/', $lineInfo['text'], $matches)) {
            $blockInfo = [
                'element' => [
                    'name' => 'blockquote',
                    'handler' => 'lines',
                    'text' => (array)$matches[1],
                ],
            ];
            return $blockInfo;
        }
    }

    protected function blockQuoteContinue($lineInfo, array $Block)
    {
        if ($lineInfo['text'][0] === '>' && preg_match('/^>[ ]?(.*)/', $lineInfo['text'], $matches)) {
            if (isset($Block['interrupted'])) {
                $Block['element']['text'] [] = '';
                unset($Block['interrupted']);
            }
            $Block['element']['text'] [] = $matches[1];
            return $Block;
        }
        if (!isset($Block['interrupted'])) {
            $Block['element']['text'] [] = $lineInfo['text'];
            return $Block;
        }
    }

    protected function blockRule($Line)
    {
        if (preg_match('/^([' . $Line['text'][0] . '])([ ]*\1){2,}[ ]*$/', $Line['text'])) {
            $Block = [
                'element' => [
                    'name' => 'hr'
                ],
            ];
            return $Block;
        }
    }

    protected function blockSetextHeader($Line, array $Block = null)
    {
        if (!isset($Block) || isset($Block['type']) || isset($Block['interrupted'])) {
            return;
        }
        if (chop($Line['text'], $Line['text'][0]) === '') {
            $Block['element']['name'] = $Line['text'][0] === '=' ? 'h1' : 'h2';
            return $Block;
        }
    }

    protected function blockMarkup($Line)
    {
        if ($this->markupEscaped) {
            return;
        }
        if (preg_match('/^<(\w*)(?:[ ]*' . $this->regexHtmlAttribute . ')*[ ]*(\/)?>/', $Line['text'], $matches)) {
            $element = strtolower($matches[1]);
            if (in_array($element, $this->textLevelElements)) {
                return;
            }
            $Block = [
                'name' => $matches[1],
                'depth' => 0,
                'markup' => $Line['text'],
            ];
            $length = strlen($matches[0]);
            $remainder = substr($Line['text'], $length);
            if (trim($remainder) === '') {
                if (isset($matches[2]) || in_array($matches[1], $this->voidElements)) {
                    $Block['closed'] = true;
                    $Block['void'] = true;
                }
            } else {
                if (isset($matches[2]) || in_array($matches[1], $this->voidElements)) {
                    return;
                }
                if (preg_match('/<\/' . $matches[1] . '>[ ]*$/i', $remainder)) {
                    $Block['closed'] = true;
                }
            }
            return $Block;
        }
    }

    protected function blockMarkupContinue($Line, array $Block)
    {
        if (isset($Block['closed'])) {
            return;
        }
        if (preg_match('/^<' . $Block['name'] . '(?:[ ]*' . $this->regexHtmlAttribute . ')*[ ]*>/i', $Line['text'])) {
            $Block['depth']++;
        }
        if (preg_match('/(.*?)<\/' . $Block['name'] . '>[ ]*$/i', $Line['text'], $matches)) {
            if ($Block['depth'] > 0) {
                $Block['depth']--;
            } else {
                $Block['closed'] = true;
            }
        }
        if (isset($Block['interrupted'])) {
            $Block['markup'] .= "\n";
            unset($Block['interrupted']);
        }
        $Block['markup'] .= "\n" . $Line['body'];
        return $Block;
    }

    protected function blockReference($lineInfo)
    {
        if (preg_match('/^\[(.+?)\]:[ ]*<?(\S+?)>?(?:[ ]+["\'(](.+)["\')])?[ ]*$/', $lineInfo['text'], $matches)) {
            $id = strtolower($matches[1]);
            $data = [
                'url' => $matches[2],
                'title' => null,
            ];
            if (isset($matches[3])) {
                $data['title'] = $matches[3];
            }
            $this->definitionData['Reference'][$id] = $data;
            return ['hidden' => true];
        }
    }

    protected function blockTable($Line, array $Block = null)
    {
        if (!isset($Block) || isset($Block['type']) || isset($Block['interrupted'])) {
            return;
        }
        if (strpos($Block['element']['text'], '|') !== false && chop($Line['text'], ' -:|') === '') {
            $alignments = [];
            $divider = $Line['text'];
            $divider = trim($divider);
            $divider = trim($divider, '|');
            $dividerCells = explode('|', $divider);
            foreach ($dividerCells as $dividerCell) {
                $dividerCell = trim($dividerCell);
                if ($dividerCell === '') {
                    continue;
                }
                $alignment = null;
                if ($dividerCell[0] === ':') {
                    $alignment = 'left';
                }
                if (substr($dividerCell, -1) === ':') {
                    $alignment = $alignment === 'left' ? 'center' : 'right';
                }
                $alignments[] = $alignment;
            }
            $HeaderElements = [];
            $header = $Block['element']['text'];
            $header = trim($header);
            $header = trim($header, '|');
            $headerCells = explode('|', $header);
            foreach ($headerCells as $index => $headerCell) {
                $headerCell = trim($headerCell);
                $HeaderElement = [
                    'name' => 'th',
                    'text' => $headerCell,
                    'handler' => 'line',
                ];
                if (isset($alignments[$index])) {
                    $alignment = $alignments[$index];

                    $HeaderElement['attributes'] = [
                        'style' => 'text-align: ' . $alignment . ';',
                    ];
                }
                $HeaderElements [] = $HeaderElement;
            }
            $Block = [
                'alignments' => $alignments,
                'identified' => true,
                'element' => [
                    'name' => 'table',
                    'handler' => 'elements',
                ],
            ];
            $Block['element']['text'][] = [
                'name' => 'thead',
                'handler' => 'elements',
            ];
            $Block['element']['text'][] = [
                'name' => 'tbody',
                'handler' => 'elements',
                'text' => [],
            ];
            $Block['element']['text'][0]['text'][] = [
                'name' => 'tr',
                'handler' => 'elements',
                'text' => $HeaderElements,
            ];
            return $Block;
        }
    }

    protected function blockTableContinue($Line, array $Block)
    {
        if (isset($Block['interrupted'])) {
            return;
        }
        if ($Line['text'][0] === '|' || strpos($Line['text'], '|')) {
            $Elements = [];
            $row = $Line['text'];
            $row = trim($row);
            $row = trim($row, '|');
            preg_match_all('/(?:(\\\\[|])|[^|`]|`[^`]+`|`)+/', $row, $matches);
            foreach ($matches[0] as $index => $cell) {
                $cell = trim($cell);
                $Element = [
                    'name' => 'td',
                    'handler' => 'line',
                    'text' => $cell,
                ];
                if (isset($Block['alignments'][$index])) {
                    $Element['attributes'] = [
                        'style' => 'text-align: ' . $Block['alignments'][$index] . ';',
                    ];
                }
                $Elements[] = $Element;
            }
            $Element = [
                'name' => 'tr',
                'handler' => 'elements',
                'text' => $Elements,
            ];
            $Block['element']['text'][1]['text'] [] = $Element;
            return $Block;
        }
    }

    protected function elements(array $Elements)
    {
        $markup = '';
        foreach ($Elements as $Element) {
            $markup .= "\n" . $this->element($Element);
        }
        $markup .= "\n";
        return $markup;
    }

    /**
     * 解析单行
     * @param $text
     * @return string
     */
    public function line($text)
    {
        $markup = '';
        # $excerpt is based on the first occurrence of a marker
        while ($excerpt = strpbrk($text, $this->inlineMarkerList)) {
            $marker = $excerpt[0];
            $markerPosition = strpos($text, $marker);
            $Excerpt = ['text' => $excerpt, 'context' => $text];
            foreach ($this->InlineTypes[$marker] as $inlineType) {
                $Inline = $this->{'inline' . $inlineType}($Excerpt);
                if (!isset($Inline)) {
                    continue;
                }
                # makes sure that the inline belongs to "our" marker
                if (isset($Inline['position']) && $Inline['position'] > $markerPosition) {
                    continue;
                }
                # sets a default inline position
                if (!isset($Inline['position'])) {
                    $Inline['position'] = $markerPosition;
                }
                # the text that comes before the inline
                $unmarkedText = substr($text, 0, $Inline['position']);
                # compile the unmarked text
                $markup .= $this->unmarkedText($unmarkedText);
                # compile the inline
                $markup .= isset($Inline['markup']) ? $Inline['markup'] : $this->element($Inline['element']);
                # remove the examined text
                $text = substr($text, $Inline['position'] + $Inline['extent']);
                continue 2;
            }
            # the marker does not belong to an inline
            $unmarkedText = substr($text, 0, $markerPosition + 1);
            $markup .= $this->unmarkedText($unmarkedText);
            $text = substr($text, $markerPosition + 1);
        }
        $markup .= $this->unmarkedText($text);
        return $markup;
    }

    protected function inlineCode($Excerpt)
    {
        $marker = $Excerpt['text'][0];
        if (preg_match('/^(' . $marker . '+)[ ]*(.+?)[ ]*(?<!' . $marker . ')\1(?!' . $marker . ')/s', $Excerpt['text'], $matches)) {
            $text = $matches[2];
            $text = htmlspecialchars($text, ENT_NOQUOTES, 'UTF-8');
            $text = preg_replace("/[ ]*\n/", ' ', $text);

            return [
                'extent' => strlen($matches[0]),
                'element' => [
                    'name' => 'code',
                    'text' => $text,
                ],
            ];
        }
    }

    protected function inlineEmailTag($Excerpt)
    {
        if (strpos($Excerpt['text'], '>') !== false and preg_match('/^<((mailto:)?\S+?@\S+?)>/i', $Excerpt['text'], $matches)) {
            $url = $matches[1];
            if (!isset($matches[2])) {
                $url = 'mailto:' . $url;
            }
            return [
                'extent' => strlen($matches[0]),
                'element' => [
                    'name' => 'a',
                    'text' => $matches[1],
                    'attributes' => [
                        'href' => $url,
                    ],
                ],
            ];
        }
    }

    protected function inlineEmphasis($Excerpt)
    {
        if (!isset($Excerpt['text'][1])) {
            return;
        }
        $marker = $Excerpt['text'][0];
        if ($Excerpt['text'][1] === $marker and preg_match($this->strongRegex[$marker], $Excerpt['text'], $matches)) {
            $emphasis = 'strong';
        } elseif (preg_match($this->emRegex[$marker], $Excerpt['text'], $matches)) {
            $emphasis = 'em';
        } else {
            return;
        }
        return [
            'extent' => strlen($matches[0]),
            'element' => [
                'name' => $emphasis,
                'handler' => 'line',
                'text' => $matches[1],
            ],
        ];
    }

    protected function inlineEscapeSequence($Excerpt)
    {
        if (isset($Excerpt['text'][1]) || in_array($Excerpt['text'][1], $this->specialCharacters)) {
            return [
                'markup' => $Excerpt['text'][1],
                'extent' => 2,
            ];
        }
    }

    protected function inlineImage($Excerpt)
    {
        if (!isset($Excerpt['text'][1]) || $Excerpt['text'][1] !== '[') {
            return;
        }
        $Excerpt['text'] = substr($Excerpt['text'], 1);
        $Link = $this->inlineLink($Excerpt);
        if ($Link === null) {
            return;
        }
        $Inline = [
            'extent' => $Link['extent'] + 1,
            'element' => [
                'name' => 'img',
                'attributes' => [
                    'src' => $Link['element']['attributes']['href'],
                    'alt' => $Link['element']['text'],
                ],
            ],
        ];
        $Inline['element']['attributes'] += $Link['element']['attributes'];
        unset($Inline['element']['attributes']['href']);
        return $Inline;
    }

    protected function inlineLink($Excerpt)
    {
        $Element = [
            'name' => 'a',
            'handler' => 'line',
            'text' => null,
            'attributes' => [
                'href' => null,
                'title' => null,
            ],
        ];
        $extent = 0;
        $remainder = $Excerpt['text'];
        if (preg_match('/\[((?:[^][]++|(?R))*+)\]/', $remainder, $matches)) {
            $Element['text'] = $matches[1];
            $extent += strlen($matches[0]);
            $remainder = substr($remainder, $extent);
        } else {
            return;
        }
        if (preg_match('/^[(]\s*+((?:[^ ()]++|[(][^ )]+[)])++)(?:[ ]+("[^"]*"|\'[^\']*\'))?\s*[)]/', $remainder, $matches)) {
            $Element['attributes']['href'] = $matches[1];
            if (isset($matches[2])) {
                $Element['attributes']['title'] = substr($matches[2], 1, -1);
            }
            $extent += strlen($matches[0]);
        } else {
            if (preg_match('/^\s*\[(.*?)\]/', $remainder, $matches)) {
                $definition = strlen($matches[1]) ? $matches[1] : $Element['text'];
                $definition = strtolower($definition);

                $extent += strlen($matches[0]);
            } else {
                $definition = strtolower($Element['text']);
            }
            if (!isset($this->definitionData['Reference'][$definition])) {
                return;
            }
            $Definition = $this->definitionData['Reference'][$definition];
            $Element['attributes']['href'] = $Definition['url'];
            $Element['attributes']['title'] = $Definition['title'];
        }

        $Element['attributes']['href'] = str_replace(['&', '<'], ['&amp;', '&lt;'], $Element['attributes']['href']);
        return [
            'extent' => $extent,
            'element' => $Element,
        ];
    }

    protected function inlineMarkup($Excerpt)
    {
        if ($this->markupEscaped || strpos($Excerpt['text'], '>') === false) {
            return;
        }
        if ($Excerpt['text'][1] === '/' && preg_match('/^<\/\w*[ ]*>/s', $Excerpt['text'], $matches)) {
            return [
                'markup' => $matches[0],
                'extent' => strlen($matches[0]),
            ];
        }
        if ($Excerpt['text'][1] === '!' && preg_match('/^<!---?[^>-](?:-?[^-])*-->/s', $Excerpt['text'], $matches)) {
            return [
                'markup' => $matches[0],
                'extent' => strlen($matches[0]),
            ];
        }
        if ($Excerpt['text'][1] !== ' ' && preg_match('/^<\w*(?:[ ]*' . $this->regexHtmlAttribute . ')*[ ]*\/?>/s', $Excerpt['text'], $matches)) {
            return [
                'markup' => $matches[0],
                'extent' => strlen($matches[0]),
            ];
        }
    }

    protected function inlineSpecialCharacter($Excerpt)
    {
        if ($Excerpt['text'][0] === '&' && !preg_match('/^&#?\w+;/', $Excerpt['text'])) {
            return [
                'markup' => '&amp;',
                'extent' => 1,
            ];
        }
        $SpecialCharacter = ['>' => 'gt', '<' => 'lt', '"' => 'quot'];
        if (isset($SpecialCharacter[$Excerpt['text'][0]])) {
            return [
                'markup' => '&' . $SpecialCharacter[$Excerpt['text'][0]] . ';',
                'extent' => 1,
            ];
        }
    }

    protected function inlineStrikethrough($Excerpt)
    {
        if (!isset($Excerpt['text'][1])) {
            return;
        }
        if ($Excerpt['text'][1] === '~' && preg_match('/^~~(?=\S)(.+?)(?<=\S)~~/', $Excerpt['text'], $matches)) {
            return [
                'extent' => strlen($matches[0]),
                'element' => [
                    'name' => 'del',
                    'text' => $matches[1],
                    'handler' => 'line',
                ],
            ];
        }
    }

    protected function inlineUrl($Excerpt)
    {
        if ($this->urlsLinked !== true || !isset($Excerpt['text'][2]) || $Excerpt['text'][2] !== '/') {
            return;
        }
        if (preg_match('/\bhttps?:[\/]{2}[^\s<]+\b\/*/ui', $Excerpt['context'], $matches, PREG_OFFSET_CAPTURE)) {
            $Inline = [
                'extent' => strlen($matches[0][0]),
                'position' => $matches[0][1],
                'element' => [
                    'name' => 'a',
                    'text' => $matches[0][0],
                    'attributes' => [
                        'href' => $matches[0][0],
                    ],
                ],
            ];
            return $Inline;
        }
    }

    protected function inlineUrlTag($Excerpt)
    {
        if (strpos($Excerpt['text'], '>') !== false && preg_match('/^<(\w+:\/{2}[^ >]+)>/i', $Excerpt['text'], $matches)) {
            $url = str_replace(['&', '<'], ['&amp;', '&lt;'], $matches[1]);
            return [
                'extent' => strlen($matches[0]),
                'element' => [
                    'name' => 'a',
                    'text' => $url,
                    'attributes' => [
                        'href' => $url,
                    ],
                ],
            ];
        }
    }

    protected function unmarkedText($text)
    {
        if ($this->breaksEnabled) {
            $text = preg_replace('/[ ]*\n/', "<br />\n", $text);
        } else {
            $text = preg_replace('/(?:[ ][ ]+|[ ]*\\\\)\n/', "<br />\n", $text);
            $text = str_replace(" \n", "\n", $text);
        }
        return $text;
    }

    protected function li($lines)
    {
        $markup = $this->lines($lines);
        $trimmedMarkup = trim($markup);
        if (!in_array('', $lines) && substr($trimmedMarkup, 0, 3) === '<p>') {
            $markup = $trimmedMarkup;
            $markup = substr($markup, 3);
            $position = strpos($markup, "</p>");
            $markup = substr_replace($markup, '', $position, 4);
        }
        return $markup;
    }
}