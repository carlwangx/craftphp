<?php
/**
 * 获取配置
 * @param string $name
 * @param null $value
 * @param string $range
 * @return null
 */
function C($name = '', $value = null, $range = '')
{
    if (is_null($value)) {
        return \Craft\Kernel\Config::get($name, $range);
    } else {
        \Craft\Kernel\Config::set($name, $value, $range);
    }
}

/**
 * 获取输入数据 支持默认值和过滤
 * @param $key
 * @param string $default
 * @param string $filter
 * @return mixed
 */
function I($key, $default = '', $filter = '')
{
    if (strpos($key, '.')) { // 指定参数来源
        list($method, $key) = explode('.', $key, 2);
    } else { // 默认为自动判断
        $method = 'param';
    }
    $filter = $filter ? $filter : 'htmlspecialchars';
    return \Craft\Kernel\Request::$method($key, $default, $filter);
}

/**
 * @param string $url
 * @param array $vars
 * @param string $anchor
 * @param array $queryArr
 * @param string $domain
 * @return string
 */
function U($url = '', $vars = [], $anchor = '', $queryArr = [], $domain = '')
{
    return \Craft\Kernel\Url::build($url, $vars, $anchor, $queryArr, $domain);
}

/**
 * @param $msg
 * @param int $code
 * @throws \Craft\Kernel\Exception
 */
function E($msg, $code = 0)
{
    throw new \Craft\Kernel\Exception($msg, $code);
}

/**
 * @param $name
 * @param string $value
 * @return mixed
 */
function session($name, $value = '')
{
    if (is_null($name)) { // 清除
        \Craft\Kernel\Session::clear($value);
    } elseif ('' === $value) { // 获取
        return \Craft\Kernel\Session::get($name);
    } elseif (is_null($value)) { // 删除session
        \Craft\Kernel\Session::delete($name);
    } else { // 设置session
        \Craft\Kernel\Session::set($name, $value);
    }
}

/**
 * @param $name
 * @param string $value
 * @return mixed
 */
function cookie($name, $value = '')
{
    if (is_null($name)) { // 清除
        \Craft\Kernel\Cookie::clear($value);
    } elseif ('' === $value) { // 获取
        return \Craft\Kernel\Cookie::get($name);
    } elseif (is_null($value)) { // 删除session
        \Craft\Kernel\Cookie::delete($name);
    } else { // 设置session
        \Craft\Kernel\Cookie::set($name, $value);
    }
}

/**
 * 控制器和url之间的转换
 * type 0  name to url； 1 url to name
 * @param string $name 字符串
 * @param integer $type 转换类型
 * @return string
 */
function name_url_trans($name, $type = 0)
{
    if ($type) {
        return preg_replace_callback( "/-([a-z])/", function ($match) {
            return ucfirst(trim($match[1], '-'));
        }, $name);
    } else {
        return strtolower(trim(preg_replace("/[A-Z]/", "-\\0", $name), '-'));
    }
}

/**
 * 数据签名认证
 * @param  array $data 被认证的数据
 * @return string       签名
 * @author carl.wang
 */
function data_auth_sign($data)
{
    //数据类型检测
    if (!is_array($data)) {
        $data = (array)$data;
    }
    ksort($data); //排序
    $code = http_build_query($data); //url编码并生成query字符串
    $sign = sha1($code); //生成签名
    return $sign;
}

/**
 * 把返回的数据集转换成Tree
 *
 * @param array $tree 要转换的数据集
 * @param string $name parent标记字段
 * @param string $value level标记字段
 * @param string $child
 *
 * @return array
 */
function tree_to_cx($tree, $name = 'name', $value = 'value', $child = 's')
{
    $cxTree = [];
    foreach ($tree as $key => $item) {
        $cxTree[$key]['n'] = $item[$name];
        $cxTree[$key]['v'] = $item[$value];
        if (!empty($item[$child])) {
            $cxTree[$key]['s'] = tree_to_cx($item[$child], $name, $value, $child);
        }

    }
    return $cxTree;
}

/**
 * 把返回的数据集转换成Tree
 * @param $list
 * @param string $pk
 * @param string $pid
 * @param string $child
 * @param int $root
 * @return array
 */
function list_to_tree($list, $pk = 'id', $pid = 'pid', $child = '_child', $root = 0)
{
    // 创建Tree
    $tree = array();
    if (is_array($list)) {
        // 创建基于主键的数组引用
        $refer = array();
        foreach ($list as $key => $data) {
            $refer[$data[$pk]] =& $list[$key];
        }
        foreach ($list as $key => $data) {
            // 判断是否存在parent
            $parentId = $data[$pid];
            if ($root == $parentId) {
                $tree[] =& $list[$key];
            } else {
                if (isset($refer[$parentId])) {
                    $parent =& $refer[$parentId];
                    $parent[$child][] =& $list[$key];
                }
            }
        }
    }
    return $tree;
}

// 加密认证数据
function encrypt_data($data, $key)
{
    return \Firebase\JWT\JWT::encode($data, $key);
}

// 加密认证数据
function decrypt_data($data, $key)
{
    return (array)\Firebase\JWT\JWT::decode($data, $key, ['HS256']);
}

function array_map_recursive($filter, $data)
{
    $result = array();
    foreach ($data as $key => $val) {
        $result[$key] = is_array($val)
            ? array_map_recursive($filter, $val)
            : call_user_func($filter, $val);
    }
    return $result;
}


/**
 * 去除代码中的空白和注释
 * @param string $content 代码内容
 * @return string
 */
function strip_whitespace($content)
{
    $stripStr = '';
    //分析php源码
    $tokens = token_get_all($content);
    $last_space = false;
    for ($i = 0, $j = count($tokens); $i < $j; $i++) {
        if (is_string($tokens[$i])) {
            $last_space = false;
            $stripStr .= $tokens[$i];
        } else {
            switch ($tokens[$i][0]) {
                //过滤各种PHP注释
                case T_COMMENT:
                case T_DOC_COMMENT:
                    break;
                //过滤空格
                case T_WHITESPACE:
                    if (!$last_space) {
                        $stripStr .= ' ';
                        $last_space = true;
                    }
                    break;
                case T_START_HEREDOC:
                    $stripStr .= "<<<THINK\n";
                    break;
                case T_END_HEREDOC:
                    $stripStr .= "THINK;\n";
                    for ($k = $i + 1; $k < $j; $k++) {
                        if (is_string($tokens[$k]) && $tokens[$k] == ';') {
                            $i = $k;
                            break;
                        } else if ($tokens[$k][0] == T_CLOSE_TAG) {
                            break;
                        }
                    }
                    break;
                default:
                    $last_space = false;
                    $stripStr .= $tokens[$i][1];
            }
        }
    }
    return $stripStr;
}

/**
 * URL重定向
 * @param string $url 重定向的URL地址
 * @param bool $permanent
 * @param string $msg 重定向前的提示信息
 * @param integer $time 重定向的等待时间（秒）
 * @return void
 */
function redirect($url, $permanent = false, $msg = '', $time = 0)
{
    //多行URL地址支持
    $url = str_replace(array("\n", "\r"), '', $url);
    if (empty($msg)) {
        $msg = "系统将在{$time}秒之后自动跳转到{$url}！";
    }
    if (!headers_sent()) {
        // redirect
        if (0 === $time) {
            header('Location: ' . $url, true, $permanent ? 301 : 302);
        } else {
            header("refresh:{$time};url={$url}", true, $permanent ? 301 : 302);
            echo($msg);
        }
        exit();
    } else {
        $str = "<meta http-equiv='Refresh' content='{$time};URL={$url}'>";
        if ($time != 0)
            $str .= $msg;
        exit($str);
    }
}

/**
 * 获取客户端IP地址
 * @param integer $type 返回类型 0 返回IP地址 1 返回IPV4地址数字
 * @param boolean $adv 是否进行高级模式获取（有可能被伪装）
 * @return mixed
 */
function get_client_ip($type = 0, $adv = true)
{
    $type = $type ? 1 : 0;
    static $ip = NULL;
    if ($ip !== NULL) return $ip[$type];
    if ($adv) {
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $arr = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            $pos = array_search('unknown', $arr);
            if (false !== $pos) unset($arr[$pos]);
            $ip = trim($arr[0]);
        } elseif (isset($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (isset($_SERVER['REMOTE_ADDR'])) {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
    } elseif (isset($_SERVER['REMOTE_ADDR'])) {
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    // IP地址合法验证
    $long = sprintf("%u", ip2long($ip));
    $ip = $long ? array($ip, $long) : array('0.0.0.0', 0);
    return $ip[$type];
}

function sql_filter(&$value)
{
    // TODO 其他安全过滤
    // 过滤查询特殊字符
    if (preg_match('/^(EXP|NEQ|GT|EGT|LT|ELT|OR|XOR|LIKE|NOTLIKE|NOT BETWEEN|NOTBETWEEN|BETWEEN|NOTIN|NOT IN|IN)$/i', $value)) {
        $value .= ' ';
    }
}

// 不区分大小写的in_array实现
function in_array_case($value, $array)
{
    return in_array(strtolower($value), array_map('strtolower', $array));
}

function get_config_value($name, $key)
{
    $config = C($name);
    return $config[$key];
}

/**
 * 检测用户是否登录
 * 使用HTTP_USER_AGENT做一个基本区分
 * @return integer 0-未登录，大于0-当前登录用户ID
 */
function is_signin()
{
    $user = session('member_info');
    if (empty($user)) {
        return 0;
    } else {
        $user['agent'] = md5($_SERVER['HTTP_USER_AGENT']);
        return session('member_auth_sign') == data_auth_sign($user) ? $user['uid'] : 0;
    }
}

/**
 * 检测管理员是否登录
 * 使用HTTP_USER_AGENT做一个基本区分
 * @return integer 0-未登录，大于0-当前登录用户ID
 */
function is_admin()
{
    $uid = cookie('uid');
    $siteId = cookie('site_id');
    $username = cookie('username');
    if (!$uid || !$siteId || !$username) {
        return 0;
    } else {
        $auth = array(
            'uid' => $uid,
            'site_id' => $siteId,
            'username' => $username,
            'agent' => md5($_SERVER['HTTP_USER_AGENT'])
        );
        return session('admin_auth_sign') == data_auth_sign($auth) ? $uid : 0;
    }
}