<?php
namespace Craft\Factory;

use Craft\Service\Crawler;


/**
 * 控制器
 * Class Controller
 * @package Api
 */
abstract class Console
{
    /** @var Logic $service */
    protected $service;

    /** @var Crawler $crawler */
    protected $crawler;

    protected $serviceName = 'Base';

    /**
     * Crawler constructor.
     */
    public function __construct()
    {
        // 实例化逻辑层
        $this->crawler = new Crawler();
        // 构造函数
        if (method_exists($this, '_init')) {
            $this->_init();
        }
    }

    protected function downloadImg($imgSrc, $saveName, $subPath, $pathRoot=UPLOAD_PATH)
    {
        $urlInfo = parse_url($imgSrc);
        $imgPathInfo = pathinfo($urlInfo['path']);
        if (!is_dir($pathRoot . $subPath)) {
            exec("mkdir -p " . $pathRoot . $subPath);
        }
        $imgUrl = $subPath . '/' . $saveName . '.' . $imgPathInfo['extension'];
        $filename = $pathRoot . $imgUrl;
        file_put_contents($filename, file_get_contents($imgSrc));
        return $imgUrl;
    }
}
