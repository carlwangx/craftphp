<?php

namespace Craft\Factory;

use Craft\Kernel\Db;
use Craft\Kernel\Exception;

abstract class Model
{
    public $status = [
        'delete'  => -1,
        'disable' => 0,
        'enable'  => 1,
        'pending' => 2,
        'unapproved' => 3,
    ];

    // 所有表的映射关系
    public $tables = [];

    /** @var  Db $db */
    protected $db;

    // 数据库配置名称
    protected $dbConfigName = 'default';

    // 模型主表
    protected $masterTable;

    protected $tableName;

    //
    public static $instances = [];

    protected function __construct()
    {
        $dbConfig = C("db_server.{$this->dbConfigName}");
        $this->db = Db::getInstance($dbConfig);
    }

    /**
     * @return static
     */
    public static function instance()
    {
        if (empty(static::$instances[static::class])) {
            static::$instances[static::class] = new static();
        }
        return static::$instances[static::class];
    }

    public function dryRun()
    {
        $this->db->dryRun = true;
    }

    public function table(string $tableName)
    {
        $this->tableName = $tableName;
        return $this;
    }

    /**
     * 查询
     * @param string $query
     * @param array $bind
     * @return mixed
     */
    protected function query(string $query, array $bind)
    {
        return $this->db->query($query, $bind);
    }

    //============  create ===============//

    /**
     * 新增数据
     * @param array $fields
     * @return mixed
     * @throws Exception
     */
    public function insert(array $fields)
    {
        $table = $this->getCurrentTableName();
        return $this->db->table($table)->insert($fields);
    }

    /**
     * @param array $fields
     * @return mixed
     * @throws Exception
     */
    public function insertWithTime(array $fields)
    {
        $fields['create_time'] = time();
        $fields['update_time'] = time();
        $table = $this->getCurrentTableName();
        return $this->db->table($table)->insert($fields);
    }

    //============  update ===============//

    /**
     * 更新数据
     * @param array $condition
     * @param array $fields
     * @param bool $isRefreshUpdateTime
     * @return bool
     * @throws Exception
     */
    public function update(array $condition, array $fields, bool $isRefreshUpdateTime = true)
    {
        if ($isRefreshUpdateTime) {
            $fields['update_time'] = time();
        }
        $table = $this->getCurrentTableName();
        return $this->db->table($table)->where($condition)->update($fields);
    }

    /**
     * @param int $id
     * @param array $fields
     * @param bool $isRefreshUpdateTime
     * @return bool|int
     * @throws Exception
     */
    public function updateById(int $id, array $fields, bool $isRefreshUpdateTime = true)
    {
        if ($isRefreshUpdateTime) {
            $fields['update_time'] = time();
        }
        $table = $this->getCurrentTableName();
        return $this->db->table($table)->where(['id'=>$id])->update($fields);
    }

    /**
     * 更新状态字段的值
     * @param $condition
     * @param $status
     * @return mixed
     * @throws Exception
     */
    public function updateStatus(array $condition, int $status)
    {
        $fields = ['status' => $status];
        $table = $this->getCurrentTableName();
        return $this->db->table($table)->where($condition)->update($fields);
    }

    /**
     * 更新状态字段的值
     * @param $id
     * @param $status
     * @return mixed
     * @throws Exception
     */
    public function updateStatusById(int $id, int $status)
    {
        $condition = ['id' => $id];
        $fields = ['status' => $status];
        $table = $this->getCurrentTableName();
        return $this->db->table($table)->where($condition)->update($fields);
    }

    /**
     * @param $id
     * @param string $field
     * @param int $step
     * @return bool
     * @throws Exception
     */
    public function incrField(int $id, string $field, int $step = 1)
    {
        $condition = [
            'id' => $id
        ];
        $table = $this->getCurrentTableName();
        return $this->db->table($table)->where($condition)->setInc($field, $step);
    }

    /**
     * 改变计数字段数量
     * @param $id
     * @param $field
     * @param int $step
     * @return mixed
     * @throws Exception
     */
    public function incrViewCount(int $id, string $field = 'view_count', int $step = 1)
    {
        $condition = [
            'id' => $id
        ];
        $table = $this->getCurrentTableName();
        return $this->db->table($table)->where($condition)->setInc($field, $step);
    }

    //============  retrieve ===============//

    /**
     * 根据条件，必须获取一条记录，否则抛出异常
     * @param array $condition
     * @param string $fields
     * @return mixed
     * @throws \Craft\Kernel\Exception
     */
    public function mustGetOne(array $condition, string $fields = '*')
    {
        $table = $this->getCurrentTableName();
        $item = $this->db->table($table)->field($fields)->where($condition)->find();
        if (!$item) {
            throw new Exception('数据不存在');
        }
        return $item;
    }

    /**
     * 获取一条记录
     * @param $condition
     * @param $fields
     * @return mixed
     * @throws Exception
     */
    public function getOne(array $condition, string $fields = '*')
    {
        $table = $this->getCurrentTableName();
        $order = ['id' =>  'desc'];
        return $this->db->table($table)->field($fields)->order($order)->where($condition)->find();
    }

    /**
     * @param $id
     * @param $fields
     * @return mixed
     * @throws Exception
     */
    public function getEnableOne(int $id, $fields = '*')
    {
        $condition = [
            'id' => $id,
            'status' => $this->status['enable']
        ];
        $order = ['id' =>  'desc'];
        $table = $this->getCurrentTableName();
        return $this->db->table($table)->field($fields)->order($order)->where($condition)->find();
    }


    /**
     * @param $id
     * @param $fields
     * @return mixed
     * @throws Exception
     */
    public function getOneById(int $id, string $fields = '*')
    {
        $condition = [
            'id' => $id,
        ];
        $table = $this->getCurrentTableName();
        return $this->db->table($table)->field($fields)->where($condition)->find();
    }

    /**
     * @param $condition
     * @param $field
     * @return mixed
     * @throws Exception
     */
    public function getValue(array $condition, string $field)
    {
        $table = $this->getCurrentTableName();
        $item = $this->db->table($table)->field($field)->where($condition)->find();
        return empty($item[$field]) ? null : $item[$field];
    }

    /**
     * @param array $condition
     * @param string $field
     * @return mixed
     * @throws Exception
     */
    public function mustGetValue(array $condition, string $field)
    {
        $table = $this->getCurrentTableName();
        $item = $this->db->table($table)->field($field)->where($condition)->find();
        if (!$item) {
            throw new Exception('数据不存在');
        }
        return $item[$field];
    }

    /**
     * 获取一条记录
     * @param $condition
     * @param $field
     * @return mixed
     * @throws Exception
     */
    public function find(array $condition, string $field = '*')
    {
        $table = $this->getCurrentTableName();
        return $this->db->table($table)->field($field)->where($condition)->find();
    }

    /**
     * 获取详情
     * @param $condition
     * @param $field
     * @param $order
     * @return mixed
     * @throws Exception
     */
    public function getOneByOrder(array $condition, string $field, array $order)
    {
        $table = $this->getCurrentTableName();
        return $this->db->table($table)->field($field)->order($order)->where($condition)->find();
    }


    /**
     * 根据分页获取数据
     * @param $page
     * @param $pageSize
     * @param $fields
     * @param array $order
     * @param array $condition
     * @return array
     * @throws Exception
     */
    public function getPage(int $page, int $pageSize, array $condition = [], string $fields = '*', $order = ['id' => 'desc'])
    {
        $table = $this->getCurrentTableName();
        $items = $this->db->table($table)->field($fields)->where($condition)->order($order)
            ->page($page, $pageSize)->select();
        $total = $this->db->table($table)->where($condition)->count();
        $totalPage = ceil($total/$pageSize);
        $page = [
            'items' => $items,
            'total' => $total,
            'totalPage' => $totalPage
        ];
        return $page;
    }


    /**
     * 获取前N条记录
     * @param string $fields
     * @param int $count
     * @param array $order
     * @param array $condition
     * @return mixed
     * @throws Exception
     */
    public function getList(int $count, array $condition = [], string $fields = '*', array $order = ['id' => 'desc'])
    {
        $table = $this->getCurrentTableName();
        return $this->db->table($table)->field($fields)->where($condition)->order($order)->limit($count)->select();
    }

    /**
     * 获取某行记录
     * @param array $condition
     * @param string $field
     * @param array $order
     * @return mixed
     * @throws Exception
     */
    public function getColumnList(array $condition, string $field, array $order = ['id' => 'desc'])
    {
        $table = $this->getCurrentTableName();
        $items = $this->db->table($table)->field($field)->where($condition)->order($order)->select();
        return $items ? array_column($items, $field) : [];
    }

    /**
     * 获取所有记录
     * @param string $fields
     * @param array $order
     * @param array $condition
     * @return mixed
     * @throws Exception
     */
    public function getAll(array $condition = [], $fields = '*', $order = ['id' => 'desc'])
    {
        $table = $this->getCurrentTableName();
        return $this->db->table($table)->field($fields)->where($condition)->order($order)->select();
    }

    /**
     * @param array $condition
     * @return bool
     * @throws Exception
     */
    public function exists(array $condition)
    {
        $table = $this->getCurrentTableName();
        return (bool)$this->db->table($table)->where($condition)->find();
    }

    /**
     * @param array $condition
     * @return mixed
     * @throws Exception
     */
    public function getTotal(array $condition)
    {
        $table = $this->getCurrentTableName();
        return $this->db->table($table)->where($condition)->count();
    }

    //============  delete ===============//

    /**
     * @param $id
     * @return mixed
     * @throws Exception
     */
    public function deleteById(int $id)
    {
        $condition = [
            'id' => $id
        ];
        $table = $this->getCurrentTableName();
        return $this->db->table($table)->where($condition)->delete();
    }

    /**
     * 删除数据
     * @param array $condition
     * @return mixed
     * @throws Exception
     */
    public function delete(array $condition)
    {
        $table = $this->getCurrentTableName();
        return $this->db->table($table)->where($condition)->delete();
    }

    public function beginTrans()
    {
        $this->db->beginTrans();
    }

    public function commit()
    {
        $this->db->commit();
    }

    public function rollback()
    {
        $this->db->rollback();
    }

    /**
     * @return string
     * @throws Exception
     */
    private function getCurrentTableName()
    {
        $table = $this->tableName ? $this->tableName : $this->masterTable;
        if (!$table) {
            throw new Exception('表名未指定');
        }
        // 重置表名
        $this->tableName = null;
        return $table;
    }
}