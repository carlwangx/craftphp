<?php
namespace Craft\Factory;

use Craft\Service\Upload;

/**
 * 逻辑处理基类
 *  该基类主要是对Model层的调用，如果需要加缓存，请根据使用的缓存类型和逻辑，在继承类中添加
 * Class Transaction
 * @package Api
 */
abstract class Logic
{
    /** @var Cache $cache */
    protected $cache = null;

    /** @var Model $model */
    protected $model = null;

    protected static $instances = [];

    /**
     * @return static
     */
    public static function instance()
    {
        if (empty(static::$instances[static::class])) {
            static::$instances[static::class] = new static();
        }
        return static::$instances[static::class];
    }

    /**
     *
     */
    public function dryRun()
    {
        $this->model->dryRun();
    }

    //============  create ===============//

    /**
     * 新增数据
     * @param array $fields
     * @return mixed
     * @throws \Craft\Kernel\Exception
     */
    public function insert(array $fields)
    {
        return $this->model->insert($fields);
    }

    /**
     * @param array $fields
     * @return mixed
     * @throws \Craft\Kernel\Exception
     */
    public function insertWithTime(array $fields)
    {
        $fields['create_time'] = time();
        $fields['update_time'] = time();
        return $this->model->insert($fields);
    }

    //============  update ===============//

    /**
     * @param $condition
     * @param $fields
     * @throws \Craft\Kernel\Exception
     */
    public function inOrUp($condition, $fields)
    {
        if (!$this->model->exists($condition)) {
            $this->model->insert($fields);
        } else {
            $this->model->update($condition, $fields);
        }
    }

    /**
     * 更新数据
     * @param array $condition
     * @param array $fields
     * @param bool $isRefreshUpdateTime
     * @return bool
     * @throws \Craft\Kernel\Exception
     */
    public function update(array $condition, array $fields, bool $isRefreshUpdateTime = true)
    {
        if ($isRefreshUpdateTime) {
            $fields['update_time'] = time();
        }
        return $this->model->update($condition, $fields);
    }

    /**
     * @param int $id
     * @param array $fields
     * @param bool $isRefreshUpdateTime
     * @return bool|int
     * @throws \Craft\Kernel\Exception
     */
    public function updateById(int $id, array $fields, bool $isRefreshUpdateTime = true)
    {
        if ($isRefreshUpdateTime) {
            $fields['update_time'] = time();
        }
        return $this->model->update(['id'=>$id], $fields);
    }

    /**
     * @param array $fields
     * @param bool $isRefreshUpdateTime
     * @return bool|int
     * @throws \Craft\Kernel\Exception
     */
    public function updateWithId(array $fields, bool $isRefreshUpdateTime = true)
    {
        $id = $fields['id'];
        unset($fields['id']);
        if ($isRefreshUpdateTime) {
            $fields['update_time'] = time();
        }
        return $this->model->update(['id'=>$id], $fields);
    }

    /**
     * 更新状态字段的值
     * @param $condition
     * @param $status
     * @return mixed
     * @throws \Craft\Kernel\Exception
     */
    public function updateStatus(array $condition, int $status)
    {
        $fields = ['status' => $status];
        return $this->model->update($condition, $fields);
    }

    /**
     * 更新状态字段的值
     * @param $id
     * @param $status
     * @return mixed
     * @throws \Craft\Kernel\Exception
     */
    public function updateStatusById(int $id, int $status)
    {
        $condition = ['id' => $id];
        $fields = ['status' => $status];
        return $this->model->update($condition, $fields);
    }

    /**
     * @param int $id
     * @param int $step
     * @return mixed
     * @throws \Craft\Kernel\Exception
     */
    public function incrViewCount(int $id, int $step = 1)
    {
        return $this->model->incrViewCount($id, 'view_count', $step);
    }

    /**
     * @param array $condition
     * @param string $fields
     * @return mixed
     * @throws \Craft\Kernel\Exception
     */
    public function getValue(array $condition, string $fields = '*')
    {
        return $this->model->getValue($condition, $fields);
    }

    /**
     * @param array $condition
     * @param string $fields
     * @return mixed
     * @throws \Craft\Kernel\Exception
     */
    public function getOne(array $condition, string $fields = '*')
    {
        return $this->model->getOne($condition, $fields);
    }

    /**
     * @param int $id
     * @param string $fields
     * @return mixed
     * @throws \Craft\Kernel\Exception
     */
    public function getOneById(int $id, string $fields = '*')
    {
        return $this->model->getOneById($id, $fields);
    }

    /**
     * @param int $id
     * @param string $fields
     * @return mixed
     * @throws \Craft\Kernel\Exception
     */
    public function getEnableOne(int $id, $fields = '*')
    {
        return $this->model->getEnableOne($id, $fields);
    }

    /**
     * 根据分页获取数据
     * @param $listRows
     * @param $pageNum
     * @param $fields
     * @param array $order
     * @param array $condition
     * @return array
     * @throws \Craft\Kernel\Exception
     */
    public function getPage(int $pageNum, int $listRows, array $condition = [], string $fields = '*', $order = ['id' => 'desc'])
    {
        return $this->model->getPage($pageNum, $listRows, $condition, $fields, $order);
    }

    /**
     * @param int $count
     * @param array $condition
     * @param string $fields
     * @param array $order
     * @return mixed
     * @throws \Craft\Kernel\Exception
     */
    public function getList(int $count, array $condition = [], string $fields = '*', array $order = ['id' => 'desc'])
    {
        return $this->model->getList($count, $condition, $fields, $order);
    }

    /**
     * @param int $count
     * @param string $fields
     * @param array $order
     * @return mixed
     * @throws \Craft\Kernel\Exception
     */
    public function getEnableList(int $count, string $fields = '*', array $order = ['id' => 'desc'])
    {
        $condition = ['status' => $this->model->status['enable']];
        return $this->model->getList($count, $condition, $fields, $order);
    }

    /**
     * @param array $condition
     * @param string $fields
     * @param array $order
     * @return mixed
     * @throws \Craft\Kernel\Exception
     */
    public function getAll(array $condition = [], $fields = '*', $order = ['id' => 'desc'])
    {
        return $this->model->getAll($condition, $fields, $order);
    }

    /**
     * @param int $id
     * @return mixed
     * @throws \Craft\Kernel\Exception
     */
    public function deleteById(int $id)
    {
        return $this->model->deleteById($id);
    }

    /**
     * @param array $condition
     * @return mixed
     * @throws \Craft\Kernel\Exception
     */
    public function delete(array $condition)
    {
        return $this->model->delete($condition);
    }

    //=============== 非数据库 ===================

    /**
     * @param $configName
     * @param $imageInfo
     * @param $maxWidth
     * @return array
     * @throws \Craft\Kernel\Exception
     */
    public function uploadImg($configName, $imageInfo, $maxWidth)
    {
        $UploadService = new Upload();
        return  $UploadService->image($configName, $imageInfo, $maxWidth);
    }
}