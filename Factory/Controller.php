<?php
namespace Craft\Factory;

use Craft\Kernel\Request;
use Craft\Kernel\Response;
use Craft\Kernel\View;
use Craft\Kernel\Url;
use Craft\Kernel\Config;
use Craft\Kernel\Cookie;

/**
 * 控制器，只做路由相关的操作，request和response放在相应的类中
 * Class Controller
 * @package Api
 */
abstract class Controller
{
    protected $request;

    protected $response;

    /** @var View $view */
    private $view;

    /**
     * 架构函数 初始化视图类
     * @access public
     */
    public function __construct()
    {
        // 实例化逻辑层
        $this->request = new Request();
        $this->response = new Response();
        // 视图类
        $this->view = new View();
        // 构造函数
        if (method_exists($this, '_init')) {
            $this->_init();
        }
    }

    /**
     * 加载模板和页面输出 可以返回输出内容
     * @access public
     * @param string $template 模板文件名
     * @return mixed
     */
    public function display($template = '')
    {
        $this->view->display($template);
    }

    /**
     *  获取输出页面内容
     * 调用内置的模板引擎fetch方法，
     * @access protected
     * @param string $templateFile 指定要调用的模板文件
     * 默认为空 由系统自动定位模板文件
     * @return string
     */
    protected function fetch($templateFile = '')
    {
        return $this->view->fetch($templateFile);
    }

    /**
     * 模板变量赋值
     * @access protected
     * @param mixed $name 要显示的模板变量
     * @param mixed $value 变量的值
     * @return void
     */
    public function assign($name, $value = '')
    {
        $this->view->assign($name, $value);
    }

    protected function sendJson($code, $data = [], $msg = '')
    {
        $this->response->ajaxReturn($code, $data, $msg);
    }

    public function sendSuccessJson($data, $msg = '', $code = 0)
    {
        $this->response->ajaxReturn($code, $data, $msg);
    }

    public function sendFailJson($msg, $code = 500, $data = [])
    {
        $this->response->ajaxReturn($code, $data, $msg);
    }

    /**
     * Action跳转(URL重定向） 支持指定模块和延时跳转
     * @access protected
     * @param string $url 跳转的URL表达式
     * @param array $params 其它URL参数
     * @param bool $permanent
     * @param string $msg 跳转提示信息
     * @param integer $delay 延时跳转的时间 单位为秒
     * @return void
     */
    protected function redirect($url, $params = [], $permanent = false, $msg = '', $delay = 0)
    {
        Url::build($url, $params);
        redirect($url, $permanent, $msg, $delay);
    }

    /**
     * 检查APP状态
     */
    protected function checkAppStatus()
    {
        if (APP_STATUS == 'upgrade' && Cookie::get('version') != 'preview') {
            $version = I('get.v');
            if ($version != 'preview') {
                $this->redirect('status/upgrade');
            }
            Cookie::set('version', 'preview');
        } elseif (APP_STATUS == 'develop') {
            $this->redirect('status/develop');
        }
    }

    // 后台登录链接
    public function generateAuthUrl()
    {
        $auth = Config::get('auth');
        $token = encrypt_data($auth['token']);
        return $auth['url'] . '?token=' . $token;
    }

    // 前台去登录页面
    public function gotoSignin()
    {
        if (!is_signin()) {
            $currentUrl = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
            if (Cookie::get('tkn')) {
                $currentUrl = urlencode($currentUrl) . '&auth=' . cookie('tkn');
            }
            $redirect = Config::get('passport.signin') . '?redirect=' . $currentUrl;
            redirect($redirect);
        }
    }

    /**
     * 空操作，用于输出404页面
     */
    public function _empty()
    {
        $this->redirect('status/error@www');
    }

    /**
     * controller或action不存在的时候
     */
    public function _route()
    {
        $this->redirect('status/notice');
    }

    //============== SEO start==============
    /**
     * 标题
     * @param string $title
     */
    public function title($title = '')
    {
        $webName = Config::get('site_title');
        if (empty($title)) {
            $title = $webName;
        } else {
            $title = $title . ' | ' . $webName;
        }
        $this->assign('title', $title);
    }

    /**
     * 关键字
     * @param string $keywords
     */
    public function keywords($keywords = '')
    {
        $webKeywords = Config::get('site_keywords');
        if (empty($keywords)) {
            $seoKeywords = $webKeywords;
        } else {
            $seoKeywords = $keywords . $webKeywords;
        }
        $this->assign('keywords', $seoKeywords);
    }

    /**
     * 描述
     * @param string $description
     */
    public function description($description = '')
    {
        if (empty($description)) {
            $seoDescription = Config::get('site_description');
        } else {
            $seoDescription = $description;
        }
        $this->assign('description', $seoDescription);
    }
    //============= SEO END ===============
}
