<?php
namespace Craft\Factory;

use Craft\Service\Redis;

/**
 * Cache层对外API
 * Class Cache
 * @package Api
 */
abstract class Cache
{
    protected $keyPrefix;

    /** @var  Redis $redis */
    protected $redis;

    protected $config;

    protected $configName = 'default';

    public function __construct()
    {
        $this->config = C('redis_server.' . $this->configName);
        $this->redis =  Redis::getInstance($this->config);
    }

    public function getKey($id)
    {
        $key = '';
        if (!empty($this->config['prefix'])) {
            $key .= $this->config['prefix'];
        }
        if ($this->keyPrefix) {
            $key .= $this->keyPrefix;
        } else {
            $key .= CONTROLLER_NAME;
        }
        return $key . $id;
    }
}