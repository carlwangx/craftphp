<?php
namespace Craft\Kernel;
/************ 定义常量 ***************/
// 定义系统常量
define('CRAFT_VERSION', '1.0beta'); //  版本信息
define('START_TIME', microtime(true));
define('START_MEM', memory_get_usage());
// 系统环境常量
define('IS_AJAX', (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'));
define('REQUEST_METHOD', !empty($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD'] : 'GET');
define('IS_GET', REQUEST_METHOD == 'GET');
define('IS_POST', REQUEST_METHOD == 'POST');
define('IS_PUT', REQUEST_METHOD == 'PUT');
define('IS_DELETE', REQUEST_METHOD == 'DELETE');
// 系统路径常量
define('FRAMEWORK_ROOT', __DIR__ . '/');
// 项目运行参数设置
defined('APP_MODE') || define('APP_MODE', 'web');
// 项目目录
define('APP_BASE', APP_PATH . 'Base/'); // 模块路径
define('APP_CONFIG', APP_PATH . 'Base/Config/'); // 模块路径
define('MODULE_PATH', APP_PATH . MODULE_NAME . '/'); // 模块路径
define('VIEW_PATH', MODULE_PATH . 'View/'); // 模块模板路径
define('UPLOAD_PATH', PROJECT_ROOT . '/source/upload/'); // 上传文件目录
// 缓存目录
define('RUNTIME_PATH', PROJECT_ROOT . 'runtime/'); // 项目运行缓存目录
define('VIEW_CACHE', RUNTIME_PATH . 'view/'); // 项目运行缓存目录
define('LOG_PATH', RUNTIME_PATH . 'logs/'); // 项目运行日志目录
define('TMP_PATH', RUNTIME_PATH . 'tmp/'); // 项目运行临时目录
/************ 注册自动加载 ****************/
require FRAMEWORK_ROOT . 'Kernel/Loader.php';
$mapping = require FRAMEWORK_ROOT . 'classes.php';
Loader::addNamespaceMap($mapping['namespace']);
Loader::addClassMap($mapping['class']);
Loader::register();
// 注册第三方库
require FRAMEWORK_ROOT . 'vendor/autoload.php';
// 加载系统/项目配置文件，函数
System::loadConfig();
System::loadFunction();
date_default_timezone_set(Config::get('app_timezone')); // 设置时区
define('APP_DEBUG', Config::get('app_debug'));
System::errorHandler();
// start
if (APP_MODE === 'cli') {
    ini_set('memory_limit', '2048M');
    // 应用URL调度
    Cli::start($argv);
} else {
    // xhprof
    if (mt_rand(1, 100) == 1) {
        //require FRAMEWORK_ROOT . '/xhprof/external/header.php';
    }
    // 应用URL调度
    Cgi::start();
}