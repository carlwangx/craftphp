Craftphp

支持三种模式：

* cli 命令行模式
* web 后端渲染html
* api 前后端分离，API形式

目录结构

* Api 对应用接口
* Kernel 处理MVC
* Service 第三方服务：email，upload，redis
* Util 依赖php扩展的程序，加密，图片等
* vendor 第三方扩展
* start.php 入口文件
