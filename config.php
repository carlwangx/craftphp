<?php
return [
    'app_status' => 'local', // app状态，永远是local, upgrade, develop
    'app_debug' => false,
    'app_timezone' => 'Asia/Singapore', // 默认时区
    'app_version' => '0.0.1', // 软件版本, 用来刷新缓存文件
    'error_page' => 'status/error', // 错误定向页面

    'app_trace' => [
        'enable' => true,
        'driver'  => 'file', // 日志记录方式，file, db
        // 日志记录级别
        'type' => [],
        'profile' => false
    ],

    'app_notice' => [  // 错误信息警告配置
        'mail' => [
            'send' => false,  // 是否通知
            'name' => 'error',
            'subject' => '系统错误,请及时修改',
            'level' => '',
            'receiver' => [
                'wangpeng1089@126.com',
            ],
        ],
    ],

    'data_auth_key' => 'carl.wang', // 数据加密的key

    'url_prefix' => '', // URL前缀

    'url_params' => [
        'name' => 'para',
        'sepr' => '-',
    ],

    'default_jsonp_handler' => 'jsonpReturn', // 默认JSONP格式返回的处理方法
    'var_jsonp_handler' => 'callback',

    // 模板配置
    'template' => [
        'strip_space' => false,       // 是否去除模板文件里面的html空格与换行
        'tpl_deny_func_list' => 'echo,exit',    // 模板引擎禁用函数
        'tpl_begin' => '{',            // 模板引擎普通标签开始标记
        'tpl_end' => '}',            // 模板引擎普通标签结束标记
        'taglib_begin' => '<',  // 标签库标签开始标记
        'taglib_end' => '>',  // 标签库标签结束标记
        'cache_type' => 'file',    // 模板编译类型
        'cache_path' => VIEW_CACHE,    // 模板缓存目录
        'cache_time' => 0,    // 模板缓存有效期 0 为永久，(以数字为值，单位:秒)
    ],

    'session' => [
        'prefix' => '',
        'type' => '',
        'auto_start' => true,  // 是否自动开启 SESSION
        'httponly'  => true,
        'secure'  => false,
    ],

    'cookie' => [
        'prefix' => '', // cookie 名称前缀
        'expire' => 0, // cookie 保存时间
        'path' => '/', // cookie 保存路径
        'domain' => '', // cookie 有效域名
        'secure' => false, //  cookie 启用安全传输
        'httponly' => true, // httponly设置
        'setcookie' => true,
    ],

    // 数据库服务器
    'db_server' => [
        'default' => [
            'driver'   => 'mysql',     // 数据库类型
            'username' => '',
            'password' => '',
            'hostname' => '',
            'database' => '',
            'hostport' => '3306',
            'charset'  => 'utf8',
        ],
    ],

    // 缓存服务器
    'redis_server' => [
        'default' => [
            'host'       => '127.0.0.1',
            'port'       => 6379,
            'password'   => '',
            'select'     => 0,
            'timeout'    => 0,
            'expire'     => 0,
            'persistent' => false,
            'prefix'     => '',
            'dryRun' => false,
        ],
    ],

    // 邮箱服务器
    'mail_server' => [
        'error' => [
            'host'      => '',
            'port'      => 25,
            'username'  => '',
            'password'  => '',
            'from_name' => '系统错误',
            'charset'   => 'utf-8',
            'debug'     => 0,
        ],
    ],

    // common
    'status' => [
        -1 => '删除',
        0 => '禁用',
        1 => '正常',
        2 => '待审核',
        3 => '未通过审核',
    ],

    'about_type' => [
        // 0 => '网站公告',
        1 => '关于我们',
        2 => '联系我们',
        3 => '服务条款',
        4 => '隐私政策',
        5 => '加入我们'
    ],

    'page_size'  => 20,

    'passport' => [
        'signin' => 'http://www.passauth.com/account/signin.html',
        'signup' => 'http://www.passauth.com/register/email.html'
    ],

    'upload_config' => [
        'image' => [
            'driver'   => 'local',
            'mimes'    => '', //允许上传的文件MiMe类型
            'maxSize'  => 2*1024*1024, //上传的文件大小限制 (0-不做限制)
            'exts'     => 'jpg,gif,png,jpeg', //允许上传的文件后缀
            'autoSub'  => true, //自动子目录保存文件
            'subName'  => ['date', 'Ymd'], //子目录创建方式，[0]-函数名，[1]-参数，多个参数使用数组
            'rootPath' => UPLOAD_PATH, //保存根路径
            'savePath' => '', //保存路径
            'saveName' => ['uniqid', ''], //上传文件命名规则，[0]-函数名，[1]-参数，多个参数使用数组
            'saveExt'  => '', //文件保存后缀，空则使用原后缀
            'replace'  => false, //存在同名是否覆盖
            'hash'     => true //是否生成hash编码
        ],
    ],

    // 上传服务器配置
    'upload_server' => [
        'ftp' => [
            'host'     => '', //服务器
            'port'     => 21, //端口
            'timeout'  => 90, //超时时间
            'username' => '', //用户名
            'password' => '', //密码 );
            'domain' => '' // 后面一定要加‘/’
        ],
        'local' => [
            'domain' => '/upload/' // 后面一定要加‘/’
        ],
        'qiniu' => [
            'app_key'    => '',
            'app_secret' => '',
            'bucket'     => '',
            'domain'     => '',
            'timeout'    => ''
        ],
    ],
];