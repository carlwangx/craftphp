<?php
namespace Craft\Service;
class Oauth {
    /**
     * 操作句柄
     * @var object
     * @access protected
     */
    static protected $handler  =    null;

    /**
     * 连接oauth
     * @access public
     * @param string $type  Oauth类型
     * @param array $options  配置数组
     * @return object
     */
    static public function instance($type,$options=[]) {
        $class = 'Service\\Oauth\\Driver\\'.ucfirst($type);
        self::$handler = new $class($options);
        return self::$handler;
    }
}
