<?php
namespace Service\Oauth\Driver;

use Service\Oauth\Driver;

class Google extends Driver
{
    /**
     * 获取requestCode的api接口
     * @var string
     */
    protected $getRequestCodeURL = 'https://accounts.google.com/o/oauth2/auth';

    /**
     * 获取access_token的api接口
     * @var string
     */
    protected $getAccessTokenURL = 'https://accounts.google.com/o/oauth2/token';

    /**
     * API根路径
     * @var string
     */
    protected $apiBase = 'https://www.googleapis.com/oauth2/v1/';

    /**
     * 组装接口调用参数 并调用接口
     * @param  string $api 微博API
     * @param  string $param 调用API的额外参数
     * @param  string $method HTTP请求方法 默认为GET
     * @return json
     */
    public function call($api, $param = '', $method = 'GET')
    {
        /*  Google 调用公共参数 */
        $params = [];
        $header = array("Authorization: Bearer {$this->token['access_token']}");

        $data = $this->http($this->url($api), $this->param($params, $param), $method, $header);
        return json_decode($data, true);
    }

    /**
     * 解析access_token方法请求后的返回值
     * @param string $result 获取access_token的方法的返回值
     */
    protected function parseToken($result)
    {
        return json_decode($result, true);
    }

    /**
     * 获取当前授权应用的openid
     * @return string
     */
    public function getOpenId()
    {
        $data = $this->call('people/me');
        return !empty($data['id']) ? $data['id'] : null;
    }

    /**
     * 获取当前登录的用户信息
     * @return array
     */
    public function getOauthInfo()
    {
        $data = $this->call('userinfo');

        if (!empty($data['id'])) {
            $userInfo['type'] = 'GOOGLE';
            $userInfo['name'] = $data['name'];
            $userInfo['nick'] = $data['name'];
            $userInfo['avatar'] = $data['picture'];
            return $userInfo;
        } else {
            E("获取Google用户信息失败：{$data}");
        }
    }

}
