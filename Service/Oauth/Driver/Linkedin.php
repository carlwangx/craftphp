<?php
namespace Service\Oauth\Driver;

use Service\Oauth\Driver;

class Linkedin extends Driver
{
    /**
     * 获取requestCode的api接口
     * @var string
     */
    protected $getRequestCodeURL = 'https://www.linkedin.com/uas/oauth2/authorization';

    /**
     * 获取access_token的api接口
     * @var string
     */
    protected $getAccessTokenURL = 'https://www.linkedin.com/uas/oauth2/accessToken';

    /**
     * API根路径
     * @var string
     */
    protected $apiBase = 'https://api.linkedin.com/v1/';

    /**
     * 组装接口调用参数 并调用接口
     * @param  string $api 微博API
     * @param  string $param 调用API的额外参数
     * @param  string $method HTTP请求方法 默认为GET
     * @return json
     */
    public function call($api, $param = '', $method = 'GET')
    {
        /*  linkedin 调用公共参数 */
        $params = ['format' => 'json'];
        $header = array("Authorization: Bearer {$this->token['access_token']}");
        $data = $this->http($this->url($api), $this->param($params, $param), $method, $header);
        return json_decode($data, true);
    }

    /**
     * 解析access_token方法请求后的返回值
     * @param $result 获取access_token的方法的返回值
     * @return mixed
     */
    protected function parseToken($result)
    {
        return json_decode($result, true);
    }

    public function getOpenId()
    {
        $data = $this->call('people/~');
        return $data['id'];
    }

    /**
     * 获取当前登录的用户信息
     * @return array
     */
    public function getOauthInfo()
    {
        $data = $this->call('people/~');
        if (!empty($data['id'])) {
//            $userInfo['type'] = 'GOOGLE';
//            $userInfo['name'] = $data['name'];
//            $userInfo['nick'] = $data['name'];
//            $userInfo['avatar'] = $data['picture'];
            return $data;
        } else {
            E("获取Linkdedin用户信息失败：{$data}");
        }
    }

}
