<?php
/**
 * 邮件相关服务
 */
namespace Craft\Service;

class Mail
{
    /**
     * 获取一个邮箱服务器的实例
     * @param $name
     * @return \PHPMailer
     * @throws \phpmailerException
     */
    public static function getInstance($name)
    {
        $config = C("mail_server.$name");
        $mail = new \PHPMailer();
        $mail->isSMTP();
        $mail->SMTPDebug = $config['debug'];
        //Ask for HTML-friendly debug output
        $mail->Debugoutput = 'html';
        // charset
        $mail->CharSet = $config['charset'];
        //Set the hostname of the mail server
        $mail->Host = $config['host'];
        //Set the SMTP port number - likely to be 25, 465 or 587
        $mail->Port = $config['port'];
        //Whether to use SMTP authentication
        $mail->SMTPAuth = true;
        //Username to use for SMTP authentication
        $mail->Username = $config['username'];
        //Password to use for SMTP authentication
        $mail->Password = $config['password'];
        //Set who the message is to be sent from
        $mail->setFrom($config['username'], $config['from_name']);
        return $mail;
    }
}