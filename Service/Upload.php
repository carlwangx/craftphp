<?php
/**
 * 上传相关服务
 */
namespace Craft\Service;

use Craft\Util\Image;
use Craft\Service\Upload\Driver;

class Upload
{
    /**
     * 图片处理上传方法
     * @param $configName
     * @param int $imageInfo
     * @param $maxWidth
     * @param string $thumbType
     * @return array
     * @throws \Craft\Kernel\Exception
     */
    public function image($configName, $imageInfo, $maxWidth, $thumbType = 'scale')
    {
        switch ($thumbType) {
            case 'scale':
                $thumbType = Image::IMAGE_THUMB_SCALE;
                break;
            case 'filled':
                $thumbType = Image::IMAGE_THUMB_FILLED;
                break;
            case 'center':
                $thumbType = Image::IMAGE_THUMB_CENTER;
                break;
            case 'northwest':
                $thumbType = Image::IMAGE_THUMB_NORTHWEST;
                break;
            case 'southeast':
                $thumbType = Image::IMAGE_THUMB_SOUTHEAST;
                break;
            case 'fixed':
                $thumbType = Image::IMAGE_THUMB_FIXED;
                break;
            default:
                $thumbType = Image::IMAGE_THUMB_SCALE;
        }
        $image = new Image(Image::IMAGE_IMAGICK);
        $image->open($imageInfo['tmp_name']); // ['file']['tmp_name']
        $picture = UPLOAD_PATH . uniqid();
        $image->thumb($maxWidth, $image->height(), $thumbType)->save($picture);
        $imageInfo['tmp_name'] = $picture;
        $imageInfo['ext'] = $image->type();
        return $this->uploadFile($imageInfo, $configName);
    }


    /**
     * 上传附件
     * @param $fileInfo
     * @param $configName
     * @return array
     */
    public function file($fileInfo, $configName = 'file')
    {
        return $this->uploadFile($fileInfo, $configName);
    }


    /**
     * 上传图片或文件
     * @param $file
     * @param $configName  image, file
     * @return array
     */
    private function uploadFile($file, $configName)
    {
        $uploadConfigs = C('upload_config');
        $uploadServer = C('upload_server');
        $uploadConfig = $uploadConfigs[$configName];
        if (empty($uploadConfig['driver'])) {
            $driver = 'Local';
            $serverConfig = $uploadServer['local'];
        } else {
            switch ($uploadConfig['driver']) {
                case 'ftp':
                    $driver = 'Ftp';
                    $serverConfig = $uploadServer['ftp'];
                    break;
                case 'qiniu':
                    $driver = 'Qiniu';
                    $serverConfig = $uploadServer['qiniu'];
                    break;
                default:
                    $driver = 'Local';
                    $serverConfig = $uploadServer['local'];
                    break;
            }
        }
        $Upload = new Driver($uploadConfig, $driver, $serverConfig);
        $file['rootpath'] = $uploadConfig['rootPath'];
        $info = $Upload->uploadOne($file);
        if ($info) {
            if (empty($serverConfig['domain'])) {
                $serverConfig['domain'] = '/';
            }
            $data['url']  = $serverConfig['domain'] . $info['name'];
            $data['info'] = $info['savename'];
            $data['name'] = $info['name'];
            $result = [true, $data];
        } else {
            $result = [false, $Upload->getError()];
        }
        @unlink($file['tmp_name']);
        return $result;
    }
}