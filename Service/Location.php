<?php
/**
 * 定位服务
 * @author carl.wang<2009wpeng@gmail.com>
 * @date: 2014-10-28
 * @version 0.0.1
 */
namespace Craft\Service;

class Location
{
    /**
     * 淘宝IP地址库 Reset API
     * @author Chunice <hrb@usa.com>
     * @param  [string] $ip [IP地址]
     * @return  string
     */
    public function getLocation($ip = '')
    {
        if (empty($ip)) {
            $ip = get_client_ip();
        }
        $taobaoUrl = "http://ip.taobao.com/service/getIpInfo.php?ip=";
        $url       = $taobaoUrl . $ip;
        $data      = self::httpRequest($url);
        return json_decode($data, true);
    }

    static private function httpRequest($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        curl_close($ch);
        if ($output === false) {
            return "cURL Error: " . curl_error($ch);
        }
        return $output;
    }
}