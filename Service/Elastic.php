<?php
/**
 * 邮件相关服务
 * @author carl.wang<2009wpeng@gmail.com>
 * @date: 2014-10-28
 * @version 0.0.1
 */
namespace Craft\Service;

class Elastic
{
    /*
     * 获取一个邮箱服务器的实例
     */
    public static function getInstance()
    {
        $config = C("elastic_server");
        require_once __DIR__ . '/Elastica/autoload.php';
        $elasticClient = new \Elastica\Client();
    }
}