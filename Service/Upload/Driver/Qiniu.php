<?php
namespace Craft\Service\Upload\Driver;

use Service\Qiniu\Config;
use Service\Qiniu\Auth;
use Service\Qiniu\Storage\UploadManager;

class Qiniu
{
    private $auth;
    private $upload;
    public function __construct($config)
    {
        $this->sk = $config['app_secret'];
        $this->ak = $config['app_key'];
        $this->domain = $config['domain'];
        $this->bucket = $config['bucket'];
        $this->timeout = isset($config['timeout'])? $config['timeout'] : 3600;
        $this->auth = new Auth($this->ak, $this->sk);
        $this->upload = new UploadManager();
    }


    public function checkRootPath($rootPath)
    {
        $this->rootPath = trim($rootPath, './') . '/';
        return true;
    }

    public function checkSavePath($savePath)
    {
        return true;
    }

    public function mkdir($savePath)
    {
        return true;
    }

    public function getError()
    {

    }

    public function save(&$file)
    {
        $uploadToken = $this->auth->UploadToken($this->bucket);
        $filePath = $file['tmp_name'];
        $file['name'] = $file['rootpath'] . $file['savepath'] . $file['savename'];
        // $key = str_replace('/', '_', $file['name']);
        $result = $this->upload->putFile($uploadToken, $file['name'], $filePath);
        //var_dump($this->upload->)
        // var_dump($key, $result, $filePath);
        return false === $result ? false : true;
    }
}
